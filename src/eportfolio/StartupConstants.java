
package eportfolio;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * @author Linh Diep
 */


public class StartupConstants {

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String UI_PROPERTIES_FILE_NAME_Vietnamese = "properties_VI.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_EPORTFOLIO = PATH_DATA + "eportfolio/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_VIDEO = "./video/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "eportfolio/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "PageEditorStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "Logo.png";
    public static String ICON_NEW = "New.png";
    public static String ICON_LOAD = "Load.png";
    public static String ICON_SAVE = "Save.png";
    public static String ICON_SAVE_AS = "SaveAs.png";
    public static String ICON_EXPORT = "Export.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_SITE = "Add.png";
    public static String ICON_REMOVE_SITE = "Remove.png";
    public static String ICON_ADD_SLIDE = "AddSlide.png";
    public static String ICON_REMOVE_SLIDE = "RemoveSlide.png";
    public static String ICON_MOVE_UP = "SlideUp.png";
    public static String ICON_MOVE_DOWN = "SlideDown.png"; 

    // UI SETTINGS
    public static String    DEFAULT_VIDEO ="DefaultVideo.MP4";
    public static String    DEFAULT_IMAGE ="Default.png";
    public static String    DEFAULT_STRING = "";
    public static int	    DEFAULT_BANNER_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_WORKSPACE = "workspace";
    // CSS - FOR THE TYPE SELECTION DIALOG
    public static String    LABEL_TYPE_SELECTION_PROMPT = "Select a type of component:";
    public static String    LABEL_TEXT_TYPE_SELECTION_PROMPT = "Select a text type:";
    public static String    INPUT_TYPE = "Input";
    public static String    TEXT_TYPE = "Text";
    public static String    VIDEO_TYPE = "Video";
    public static String    SLIDESHOW_TYPE = "Slideshow";
    public static String    IMAGE_TYPE = "Image";
    public static String    HEADER_TYPE = "Header";
    public static String    PARA_TYPE = "Paragraph";
    public static String    LIST_TYPE = "List";
    

    // CSS - FOR THE TOOLBARS
    public static String    CSS_CLASS_FILE_TOOLBAR_PANE = "file_toolbar_pane";
    public static String    CSS_CLASS_FILE_TOOLBAR_BUTTON = "file_toolbar_button";
    public static String    CSS_CLASS_MODE_TOOLBAR_PANE = "mode_toolbar_pane";
    public static String    CSS_CLASS_MODE_TOOLBAR_TAB = "mode_toolbar_tab";
    public static String    CSS_CLASS_PAGE_TOOLBAR_PANE = "site_toolbar_pane";
    public static String    CSS_CLASS_PAGE_TOOLBAR_BUTTON = "site_toolbar_button";
    public static String    CSS_CLASS_PAGE_PANE = "page_pane";
    public static String    CSS_CLASS_SELECTED_PAGE_PANE = "selected_page_pane";
    public static String    CSS_CLASS_PAGE_LIST_PANE = "page_list_pane";
    public static String    CSS_CLASS_BANNER_BUTTON = "banner_button";
    public static String    CSS_CLASS_COMPONENT_BUTTON = "component_button";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_EDIT = "editor_workspace_edit";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_CONTROL = "editor_workspace_control";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_PANE = "editor_workspace_component_pane";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_COMPONENT= "editor_workspace_component";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_SELECTED_COMPONENT = "editor_workspace_selected_component";
    public static String    CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_TEXT = "editor_workspace_component_text";
    // CSS - SLIDESHOW EDITING
  
    public static String    CSS_CLASS_SLIDESHOW_TOOLBAR_PANE = "slideshow_toolbar_pane";
    public static String    CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON = "slideshow_toolbar_button";
    public static String    CSS_CLASS_CAPTION_PROMPT = "caption_prompt";
    public static String    CSS_CLASS_SLIDESHOW_SETTING_PANE = "slideshow_setting_pane";
    public static String    CSS_CLASS_SLIDESHOW_WORKSPACE = "slideshow_workspace";
    public static String    CSS_CLASS_SLIDES_EDITOR_PANE = "slides_editor_pane";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_slide_edit_view";
    
    // COMPONENTS
    
   
    // CSS - COMPONENTS
    public static String    CSS_CLASS_COMPONENT_DIALOG = "component_dialog";
    public static String    CSS_CLASS_COMPONENT_DIALOG_INNER = "component_dialog_inner";
    public static String    CSS_CLASS_CHOOSE_BUTTON = "choose_button";
    public static String    CSS_CLASS_HYPERLINK_BUTTON = "hyperlink_button";
    public static String    CSS_CLASS_SAVE_COMP_BUTTON = "save_comp_button";
    public static String    CSS_CLASS_LIST_CONTROL = "list_control";
    public static String    CSS_CLASS_LIST_SCROLLBAR = "list_scrollbar";
    public static String    CSS_CLASS_SELECTED_LIST_POINT = "selected_list_point";
    public static String    CSS_CLASS_LIST_POINT = "list_point"; 
}
