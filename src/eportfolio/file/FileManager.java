/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.file;

import static eportfolio.StartupConstants.HEADER_TYPE;
import static eportfolio.StartupConstants.IMAGE_TYPE;
import static eportfolio.StartupConstants.PARA_TYPE;
import static eportfolio.StartupConstants.PATH_EPORTFOLIO;
import static eportfolio.StartupConstants.SLIDESHOW_TYPE;
import static eportfolio.StartupConstants.VIDEO_TYPE;
import eportfolio.model.Component;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.ImageModel;
import eportfolio.model.ListPointModel;
import eportfolio.model.PageModel;
import eportfolio.model.SlideshowModel;
import eportfolio.model.TextModel;
import eportfolio.model.VideoModel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Linh
 */
public class FileManager {
    
    // JSON FILE READING AND WRITING CONSTANTS

    public static String JSON_STUDENT_NAME = "student_name";
    public static String JSON_PAGES = "pages";
    public static String JSON_TITLE = "title";
    public static String JSON_LAYOUT = "template_layout";
    public static String JSON_COLOR = "template_color";
    public static String JSON_BANNER_FILENAME = "banner_file_name";
    public static String JSON_BANNER_PATH = "banner_path";
    public static String JSON_FONT = "font_family";
    public static String JSON_SIZE = "font_size";
    public static String JSON_STYLE = "font_style";
    public static String JSON_FOOTER = "footer";
    public static String JSON_COMPONENTS = "components";
    public static String JSON_COMPONENT_TYPE = "component";
    public static String JSON_COMPONENT_TEXT_TYPE = "type";
    public static String JSON_TEXT_CONTENT = "content";
    public static String JSON_LIST_POINT = "elem";
    public static String JSON_WIDTH = "width";
    public static String JSON_HEIGHT = "height";
    public static String JSON_SLIDES = "slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_VIDEO_FILE_NAME = "video_file_name";
    public static String JSON_VIDEO_PATH = "video_path";
    public static String JSON_IMAGE_POSITION = "image_position";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEPortfolio(EPortfolioModel eportfolioToSave) throws IOException {
	StringWriter sw = new StringWriter();
	// BUILD THE SLIDES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(eportfolioToSave.getPages());
	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject eportfolioJsonObject = Json.createObjectBuilder()
		.add(JSON_STUDENT_NAME, eportfolioToSave.getStudentName())
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(eportfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String eportfolioTitle = "" + eportfolioToSave.getTitle();
	String jsonFilePath = PATH_EPORTFOLIO + SLASH + eportfolioTitle + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(eportfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     *
     * @throws IOException
     */
    public void loadEportfolio(EPortfolioModel eportfolioToLoad, String jsonFilePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(jsonFilePath);
	// NOW LOAD THE COURSE
	eportfolioToLoad.reset();
	eportfolioToLoad.setStudentName(json.getString(JSON_STUDENT_NAME));
	
        if(json.getJsonArray(JSON_PAGES) == null){
        }
        else{
            JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
            for (int i = 0; i < jsonPagesArray.size(); i++) {
                JsonObject pageJso = jsonPagesArray.getJsonObject(i);
                eportfolioToLoad.addPage(pageJso.getString(JSON_TITLE),
                                        pageJso.getString(JSON_FOOTER),
                                        pageJso.getString(JSON_LAYOUT),
                                        pageJso.getString(JSON_COLOR),
                                        pageJso.getString(JSON_FONT),
                                        pageJso.getString(JSON_BANNER_FILENAME),
                                        pageJso.getString(JSON_BANNER_PATH));
                PageModel page = eportfolioToLoad.getSelectedPage();
                if(json.getJsonArray(JSON_COMPONENTS) == null){
                    System.out.println("KO");
                }
                else{
                JsonArray jsonComponentsArray = json.getJsonArray(JSON_COMPONENTS);
                    for (int j = 0; j < jsonComponentsArray.size(); j++) {
                        JsonObject componentJso = jsonComponentsArray.getJsonObject(j);
                        if (componentJso.getString(JSON_COMPONENT_TYPE).equals(IMAGE_TYPE)){
                            page.addComponent(componentJso.getString(JSON_COMPONENT_TYPE),"");
                            ImageModel image = (ImageModel)page.getSelectedComponent();
                            image.setImage(componentJso.getString(JSON_IMAGE_PATH),componentJso.getString(JSON_IMAGE_FILE_NAME));
                            image.setCaption(componentJso.getString(JSON_CAPTION));
                            image.setPosition(componentJso.getString(JSON_IMAGE_POSITION));
                            image.setWidth(componentJso.getString(JSON_WIDTH));
                            image.setHeight(componentJso.getString(JSON_HEIGHT));
                        }
                        else if (componentJso.getString(JSON_COMPONENT_TYPE).equals(VIDEO_TYPE)){
                            page.addComponent(componentJso.getString(JSON_COMPONENT_TYPE),"");
                            VideoModel video = (VideoModel)page.getSelectedComponent();
                            video.setVideo(componentJso.getString(JSON_VIDEO_PATH),componentJso.getString(JSON_VIDEO_FILE_NAME));
                            video.setCaption(componentJso.getString(JSON_CAPTION));
                            video.setWidth(componentJso.getString(JSON_WIDTH));
                            video.setHeight(componentJso.getString(JSON_HEIGHT));
                        }
                        else if (componentJso.getString(JSON_COMPONENT_TYPE).equals(SLIDESHOW_TYPE)){
                            page.addComponent(componentJso.getString(JSON_COMPONENT_TYPE),"");
                            SlideshowModel slideshow = (SlideshowModel)page.getSelectedComponent();
                            slideshow.setTitle(componentJso.getString(JSON_TITLE));
                            slideshow.setWidth(componentJso.getString(JSON_WIDTH));
                            slideshow.setHeight(componentJso.getString(JSON_HEIGHT));
                            
                            if(json.getJsonArray(JSON_SLIDES) == null){
                            }
                            else{
                                JsonArray jsonSlidesArray = json.getJsonArray(JSON_SLIDES);
                                for (int k = 0; k < jsonSlidesArray.size(); k++) {
                                    JsonObject slideJso = jsonSlidesArray.getJsonObject(i);
                                    slideshow.addSlide();
                                    ImageModel slide = (ImageModel)slideshow.getSelectedSlide();
                                    slide.setImageFileName(slideJso.getString(JSON_IMAGE_FILE_NAME));
                                    slide.setImagePath(slideJso.getString(JSON_IMAGE_PATH));
                                    slide.setCaption(slideJso.getString(JSON_CAPTION));
                                }
                            }
                        }
                        else{
                            page.addComponent(componentJso.getString(JSON_COMPONENT_TYPE),componentJso.getString(JSON_COMPONENT_TEXT_TYPE));
                            TextModel text = (TextModel)page.getSelectedComponent();
                            text.setTextType(componentJso.getString(JSON_COMPONENT_TEXT_TYPE));
                            text.setFontFamily(componentJso.getString(JSON_FONT));
                            text.setFontSize(componentJso.getString(JSON_SIZE));
                            text.setFontStyle(componentJso.getString(JSON_STYLE));
                            if(componentJso.getString(JSON_COMPONENT_TEXT_TYPE).equals(HEADER_TYPE) ||
                               componentJso.getString(JSON_COMPONENT_TEXT_TYPE).equals(PARA_TYPE) ){
                                text.setContent(componentJso.getString(JSON_TEXT_CONTENT));
                            }
                            else {
                                if (json.getJsonArray(JSON_TEXT_CONTENT) == null){
                                }
                                else{
                                    JsonArray jsonListArray = json.getJsonArray(JSON_TEXT_CONTENT);
                                    for (int h = 0; h < jsonListArray.size(); h++) {
                                        JsonObject listPointJso = jsonListArray.getJsonObject(h);
                                        ListPointModel listpoint = new ListPointModel (listPointJso.getString(JSON_LIST_POINT));
                                        text.addListPoint(listpoint);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    
    
    
    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }

    private JsonArray makePagesJsonArray(List<PageModel> pages) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (PageModel page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    private JsonObject makePageJsonObject(PageModel page) {
        // BUILD THE COMPONENT ARRAY
	JsonArray componentsJsonArray = makeComponentsJsonArray(page.getComponents());
	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject componentsJsonObject = Json.createObjectBuilder()
		.add(JSON_TITLE, page.getTitle())
                .add(JSON_LAYOUT, page.getLayout())
                .add(JSON_COLOR, page.getColor())
                .add(JSON_BANNER_FILENAME, page.getBannerFileName())
                .add(JSON_BANNER_PATH,page.getBannerPath())
                .add(JSON_FONT , page.getFont())
                .add(JSON_FOOTER, page.getFooter())
		.add(JSON_COMPONENTS, componentsJsonArray)
		.build();
        return componentsJsonObject;
    }
    
     private JsonArray makeComponentsJsonArray(List<Component> components) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Component comp : components) {
	    JsonObject jso = makeComponentsJsonObject(comp);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }
     
    private JsonObject makeComponentsJsonObject(Component component) {
        if(component.getType().equals(IMAGE_TYPE)){
            ImageModel img = (ImageModel)component;
            JsonObject jso = Json.createObjectBuilder()
                .add(JSON_COMPONENT_TYPE, img.getType())
		.add(JSON_IMAGE_FILE_NAME, img.getImageFileName())
		.add(JSON_IMAGE_PATH, img.getImagePath())
		.add(JSON_CAPTION, img.getCaption())
                .add(JSON_IMAGE_POSITION, img.getPosition())
                .add(JSON_WIDTH, img.getWidth())
                .add(JSON_HEIGHT, img.getHeight())
		.build();
            return jso;
        }
        else if(component.getType().equals(VIDEO_TYPE)){
            VideoModel vid = (VideoModel)component;
            JsonObject jso = Json.createObjectBuilder()
                .add(JSON_COMPONENT_TYPE, vid.getType())
		.add(JSON_VIDEO_FILE_NAME, vid.getVideoFileName())
		.add(JSON_VIDEO_PATH, vid.getVideoPath())
		.add(JSON_CAPTION, vid.getCaption())
                .add(JSON_WIDTH, vid.getWidth())
                .add(JSON_HEIGHT, vid.getHeight())
		.build();
            return jso;
        }
        else if (component.getType().equals(SLIDESHOW_TYPE)){
            SlideshowModel slideshow = (SlideshowModel)component;
            JsonArray slidesJsonArray = makeSlidesJsonArray(slideshow.getSlides());
            JsonObject jso = Json.createObjectBuilder()
                    .add(JSON_COMPONENT_TYPE, slideshow.getType())
                    .add(JSON_TITLE, slideshow.getTitle())
                    .add(JSON_WIDTH, slideshow.getWidth())
                    .add(JSON_HEIGHT, slideshow.getHeight())
                    .add(JSON_SLIDES, slidesJsonArray)
                    .build();
            return jso;
        }
        else{
            TextModel text = (TextModel)component;
            if(text.getTextType().equals(HEADER_TYPE) || text.getTextType().equals(PARA_TYPE)){
                JsonObject jso = Json.createObjectBuilder()
                .add(JSON_COMPONENT_TYPE, text.getType())
                .add(JSON_COMPONENT_TEXT_TYPE, text.getTextType())
		.add(JSON_FONT, text.getFontFamily())
		.add(JSON_STYLE, text.getFontStyle())
		.add(JSON_SIZE, text.getFontSize())
                .add(JSON_TEXT_CONTENT, text.getContent())
		.build();
                return jso;
            }
            else {
                JsonArray listJsonArray = makeListJsonArray(text.getListContent());
                JsonObject jso = Json.createObjectBuilder()
                .add(JSON_COMPONENT_TYPE, text.getType())
		.add(JSON_COMPONENT_TEXT_TYPE, text.getTextType())
		.add(JSON_FONT, text.getFontFamily())
		.add(JSON_STYLE, text.getFontStyle())
		.add(JSON_SIZE, text.getFontSize())
                .add(JSON_TEXT_CONTENT, listJsonArray)
		.build();
            return jso;
            }
        }
    }
       
    
    private JsonArray makeSlidesJsonArray(List<ImageModel> slides) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (ImageModel slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }
    private JsonObject makeSlideJsonObject(ImageModel slide) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
		.add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
    
    
    private JsonArray makeListJsonArray(List<ListPointModel> list) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (ListPointModel listPoint : list) {
	    JsonObject jso = makeListJsonObject(listPoint);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }
     
    private JsonObject makeListJsonObject(ListPointModel listPoint) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_LIST_POINT, listPoint.getListPoint())
		.build();
	return jso;
    }
}
