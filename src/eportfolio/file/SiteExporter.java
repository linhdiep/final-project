/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.file;

import static eportfolio.StartupConstants.IMAGE_TYPE;
import static eportfolio.StartupConstants.SLIDESHOW_TYPE;
import static eportfolio.StartupConstants.VIDEO_TYPE;
import eportfolio.model.Component;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.ImageModel;
import eportfolio.model.PageModel;
import eportfolio.model.SlideshowModel;
import eportfolio.model.VideoModel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 *
 * @author Linh
 */
public class SiteExporter {
        // WE'LL USE THIS TO BUILD PATHS
    public static String SLASH = "/";
    public static String JSON_EXT = ".json";
    public static String HTML_EXT = ".html";

    // HERE ARE THE DIRECTORIES WE CARE ABOUT
    public static String BASE_DIR = "./base/";
    public static String SITES_DIR = "./sites/";
    public static String CSS_DIR = "css/";
    public static String DATA_DIR = "data/";
    public static String EPORTFOLIO_DIR = DATA_DIR + "eportfolio/";
    public static String ICONS_DIR = "icons/";
    public static String IMG_DIR = "img/";
    public static String VIDEO_DIR = "video/";
    public static String SLIDESHOW_DIR = "slideshow/";
    public static String JS_DIR = "js/";

    // AND HERE ARE THE FILES WE CARE ABOUT
    public static String INDEX_FILE = "index.html";
    public static String STYLESHEET_FILE = "style.css";
    public static String LAYOUT1 = "layout1.css";
    public static String LAYOUT2 = "layout2.css";
    public static String LAYOUT3 = "layout3.css";
    public static String LAYOUT4 = "layout4.css";
    public static String LAYOUT5 = "layout5.css";
    public static String JS_FILE = "page.js";
    public static String DATA_FILE = "json_file.json";

    public void exportSite(EPortfolioModel eportfolioToExport) throws IOException {
        File sites = new File(SITES_DIR);
        if (!sites.exists()) {
           sites.mkdir();
        }
        if (eportfolioToExport.getTitle()== null){
            eportfolioToExport.setTitle(eportfolioToExport.getStudentName());
        }
	// THE SITE HOME PATH
	String homeSitePath = SITES_DIR + eportfolioToExport.getTitle() + SLASH;
        File siteDir = new File(homeSitePath);
        if (siteDir.exists())
	    deleteDir(siteDir);
        
        siteDir.mkdir();
        
	// MAKE THE CSS, DATA, IMG, AND JS DIRECTORIES
	new File(homeSitePath + CSS_DIR).mkdir();
        new File(homeSitePath + JS_DIR).mkdir();
	new File(homeSitePath + DATA_DIR).mkdir();
	new File(homeSitePath + ICONS_DIR).mkdir();
	new File(homeSitePath + IMG_DIR).mkdir();
        new File(homeSitePath + VIDEO_DIR).mkdir();
        new File(homeSitePath + SLIDESHOW_DIR).mkdir();
	
        //HTML 
        
	int numOfPages = eportfolioToExport.getPages().size();
        for (int i = 1; i<= numOfPages; i++ ){
            Path htmlSrcPath = new File(BASE_DIR + INDEX_FILE).toPath();
            Path htmlDestPath = new File(homeSitePath + i + HTML_EXT).toPath();
            Files.copy(htmlSrcPath, htmlDestPath,StandardCopyOption.REPLACE_EXISTING);
        }
        
	// NOW COPY OVER THE HTML, CSS, ICON, AND JAVASCRIPT FILES
	copyAllFiles(BASE_DIR, homeSitePath);
	copyAllFiles(BASE_DIR + CSS_DIR, homeSitePath + CSS_DIR);
	copyAllFiles(BASE_DIR + ICONS_DIR, homeSitePath + ICONS_DIR);
	copyAllFiles(BASE_DIR + JS_DIR, homeSitePath + JS_DIR);

	// NOW FOR THE TWO THINGS THAT WE HAVE TO COPY OVER EVERY TIME,
	// NAMELY, THE DATA FILE, THE VIDEOS, THE IMAGES, AND THE SLIDESHOWS IMAGES
	// FIRST COPY THE DATA FILE
	Path dataSrcPath = new File(EPORTFOLIO_DIR + eportfolioToExport.getTitle() + JSON_EXT).toPath();
	Path dataDestPath = new File(homeSitePath + DATA_DIR + DATA_FILE).toPath();
	Files.copy(dataSrcPath, dataDestPath,StandardCopyOption.REPLACE_EXISTING);
        
	// AND NOW ALL THE IMAGES, VIDEOS, SLIDESHOW IMAGE
	for (PageModel page : eportfolioToExport.getPages()) {
            Path srcBannerPath = new File(page.getBannerPath() + SLASH + page.getBannerFileName()).toPath();
            Path destBannerPath = new File(homeSitePath + IMG_DIR + page.getBannerFileName()).toPath();
            Files.copy(srcBannerPath, destBannerPath,StandardCopyOption.REPLACE_EXISTING);
            for(Component component : page.getComponents()){
                if(component.getType().equals(IMAGE_TYPE)){
                    ImageModel img = (ImageModel)component;
                    Path srcImgPath = new File(img.getImagePath() + SLASH + img.getImageFileName()).toPath();
                    Path destImgPath = new File(homeSitePath + IMG_DIR + img.getImageFileName()).toPath();
                    Files.copy(srcImgPath, destImgPath,StandardCopyOption.REPLACE_EXISTING);
                }
                else if (component.getType().equals(VIDEO_TYPE)){
                    VideoModel vid = (VideoModel)component;
                    Path srcVidPath = new File(vid.getVideoPath() + SLASH + vid.getVideoFileName()).toPath();
                    Path destVidPath = new File(homeSitePath + VIDEO_DIR + vid.getVideoFileName()).toPath();
                    Files.copy(srcVidPath, destVidPath,StandardCopyOption.REPLACE_EXISTING);
                }
                else if (component.getType().equals(SLIDESHOW_TYPE)){
                    SlideshowModel slideshow = (SlideshowModel)component;
                    for (ImageModel slide : slideshow.getSlides()) {
                        Path srcSlidePath = new File(slide.getImagePath() + SLASH + slide.getImageFileName()).toPath();
                        Path destSlidePath = new File(homeSitePath + IMG_DIR + slide.getImageFileName()).toPath();
                        Files.copy(srcSlidePath, destSlidePath,StandardCopyOption.REPLACE_EXISTING);
                    }
                }
            }
        }
    }
    
    public void deleteDir(File dir) {
	File[] files = dir.listFiles();
	for (File f : files) {
	    if (f.isDirectory()) {
		deleteDir(f);
		f.delete();
	    }
	    else
		f.delete();
	}
	dir.delete();
    }

    public void copyAllFiles(String sourceFile, String destinationDir) throws IOException {
	File srcDir = new File(sourceFile);
	File[] files = srcDir.listFiles();
	for (File f : files) {
	    Path srcPath = f.toPath();
	    Path newPath = new File(destinationDir).toPath();
	    if (!f.isDirectory()) {
		Files.copy(srcPath, newPath.resolve(srcPath.getFileName()),StandardCopyOption.REPLACE_EXISTING);
	    }
	}
    }
}
