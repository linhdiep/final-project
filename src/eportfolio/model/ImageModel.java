/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import static eportfolio.StartupConstants.IMAGE_TYPE;
import javafx.stage.Stage;

/**
 *
 * @author Linh
 */
public class ImageModel  extends Component{
    Stage componentView;
    String imageFileName;
    String imagePath;
    String caption, position;
    String width, height;
     
    /**
     * Constructor, it initializes all slide data.
     */
    public ImageModel(String initImageFileName, String initImagePath,
                        String initCaption, String initPosition, String initWidth, String initHeight) {
        imageFileName = initImageFileName;
        imagePath = initImagePath;
        caption = initCaption;
        position = initPosition;
        width = initWidth;
        height = initHeight;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption; }
    public String getPosition() { return position; }
    public String getWidth() { return width; }
    public String getHeight() { return height; }
    public String getType(){
        return IMAGE_TYPE;
    }
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setCaption(String initCaption) {
	caption = initCaption;
    }
    public void setPosition(String initPosition) {
	position = initPosition;
    }
    public void setWidth(String initWidth) {
	width = initWidth;
    }
     public void setHeight(String initHeight) {
	height = initHeight;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
}
