/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import static eportfolio.StartupConstants.VIDEO_TYPE;
import javafx.stage.Stage;

/**
 *
 * @author Linh
 */
public class VideoModel  extends Component {
    Stage componentView;
    String videoFileName;
    String videoPath;
    String caption;
    String width, height;
     
    /**
     * Constructor, it initializes all slide data.
     */
    public VideoModel(String initVideoFileName, String initVideoPath,
                        String initCaption,  String initWidth, String initHeight) {
        videoFileName = initVideoFileName;
        videoPath = initVideoPath;
        caption = initCaption;
        width = initWidth;
        height = initHeight;
    }
    // ACCESSOR METHODS
    public String getVideoFileName() { return videoFileName; }
    public String getVideoPath() { return videoPath; }
    public String getCaption() { return caption; }
    public String getWidth() { return width; }
    public String getHeight() { return height; }
    public String getType(){
        return VIDEO_TYPE;
    }
    // MUTATOR METHODS
    public void setImageFileName(String initVideoFileName) {
	videoFileName = initVideoFileName;
    }
    
    public void setImagePath(String initVideoPath) {
	videoPath = initVideoPath;
    }
    
    public void setCaption(String initCaption) {
	caption = initCaption;
    }
    public void setWidth(String initWidth) {
	width = initWidth;
    }
     public void setHeight(String initHeight) {
	height = initHeight;
    }
    
    public void setVideo(String initPath, String initFileName) {
	videoPath = initPath;
	videoFileName = initFileName;
    }
}
