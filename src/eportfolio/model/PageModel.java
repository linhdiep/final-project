/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import static eportfolio.StartupConstants.DEFAULT_IMAGE;
import static eportfolio.StartupConstants.DEFAULT_STRING;
import static eportfolio.StartupConstants.DEFAULT_VIDEO;
import static eportfolio.StartupConstants.IMAGE_TYPE;
import static eportfolio.StartupConstants.LIST_TYPE;
import static eportfolio.StartupConstants.PATH_IMAGES;
import static eportfolio.StartupConstants.PATH_VIDEO;
import static eportfolio.StartupConstants.TEXT_TYPE;
import static eportfolio.StartupConstants.VIDEO_TYPE;
import eportfolio.view.PageEditorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

/**
 *
 * @author Linh
 */
public class PageModel {
    int numOfPage;
    String title;
    String footer;
    String layout;
    String color;
    String font;
    String bannerFileName;
    String bannerPath;
    Component selectedComponent;
    ObservableList<Component> components;
    PageEditorView pageView;
    public PageModel(String initTitle, String initFooter, String initLayout, String initColor,
                    String initFont, String initBannerFileName, String initBannerPath){
        title = initTitle;
        footer = initFooter;
        layout = initLayout;
        color = initColor;
        font = initFont;
        bannerFileName = initBannerFileName;
        bannerPath = initBannerPath;
	components = FXCollections.observableArrayList();
        reset();
	
    }
     // ACCESSOR METHODS
    public boolean isComponentSelected() {
	return selectedComponent != null;
    }
    
    public boolean isSelectedComponent(Component testComponent) {
	return selectedComponent == testComponent;
    }
    
    public ObservableList<Component> getComponents() {
	return components;
    }
    
    public Component getSelectedComponent() {
	return selectedComponent;
    }
    public int getNum(){
        return numOfPage;
    }
    public String getTitle() {
	return title; 
    }
    public String getFooter() {
        return footer; 
    }
    public String getLayout(){
        return layout;
    }
    public String getColor(){
        return color;
    }
     public String getFont(){
        return font;
    }
    public String getBannerFileName(){
        return bannerFileName;
    }
    public String getBannerPath(){
        return bannerPath;
    }
    // MUTATOR METHODS
    public void setNum(int initNumOfPage){
        numOfPage = initNumOfPage;
    }
    public void setSelectedComponent(Component initSelectedComponent) {
	selectedComponent = initSelectedComponent;
    }
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    public void setFooter(String initFooter) {
        footer = initFooter;
    }
    public void setLayout(String initLayout){
        layout = initLayout;
    }
    public void setColor(String initColor){
        color = initColor;
    }
    public void setFont(String initFont){
        font = initFont;
    }
    public void setBannerFileName(String initBannerFileName){
        bannerFileName = initBannerFileName;
    }
    public void setBannerPath(String initBannerPath){
        bannerPath = initBannerPath;
    }
    public void setBanner(String initBannerPath,String initBannerFileName){
        bannerFileName = initBannerFileName;
        bannerPath = initBannerPath;
    }
    
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	components.clear();
	selectedComponent = null;
    }

    /**
     * Adds a page to the portfolio with the parameter settings.
     **/
    public void addComponent(String type, String subtype) {
        if(type.equals(IMAGE_TYPE)){
            ImageModel imgComp = new ImageModel(DEFAULT_IMAGE,PATH_IMAGES, DEFAULT_STRING, DEFAULT_STRING,
                                                DEFAULT_STRING,DEFAULT_STRING);
            components.add(imgComp);
            setSelectedComponent(imgComp);
        }
        else if (type.equals(VIDEO_TYPE)){
            VideoModel videoComp = new VideoModel(DEFAULT_VIDEO,PATH_VIDEO, DEFAULT_STRING,
                                                 DEFAULT_STRING,DEFAULT_STRING);
            components.add(videoComp);
            setSelectedComponent(videoComp);
        }
        else if (type.equals(TEXT_TYPE)){
            TextModel textComp = new TextModel(subtype,DEFAULT_STRING,DEFAULT_STRING,DEFAULT_STRING);
            components.add(textComp);
            setSelectedComponent(textComp);
        }
        else{
            SlideshowModel slideshowComp = new SlideshowModel(DEFAULT_STRING,DEFAULT_STRING,DEFAULT_STRING);
            components.add(slideshowComp);
            setSelectedComponent(slideshowComp);
        }
    }

    /**
     * Removes the currently selected page from the portfolio and
     * updates the display.
     */
    public void removeSelectedComponent() {
	if (isComponentSelected()) {
	    components.remove(selectedComponent);
	    selectedComponent = null;
	}
    }
    

}
