/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import static eportfolio.StartupConstants.DEFAULT_IMAGE;
import static eportfolio.StartupConstants.DEFAULT_STRING;
import static eportfolio.StartupConstants.PATH_IMAGES;
import static eportfolio.StartupConstants.SLIDESHOW_TYPE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

/**
 *
 * @author Linh
 */
public class SlideshowModel extends Component{
    String title;
    ObservableList<ImageModel> slides;
    ImageModel selectedSlide;
    String width, height;
    
    public SlideshowModel(String initTitle, String initWidth, String initHeight) {
        title = initTitle;
        width = initWidth;
        height = initHeight;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public boolean isSelectedSlide(ImageModel testSlide) {
	return selectedSlide == testSlide;
    }
    
    public ObservableList<ImageModel> getSlides() {
	return slides;
    }
    
    public ImageModel getSelectedSlide() {
	return selectedSlide;
    }
    public String getType(){
        return SLIDESHOW_TYPE;
    }
    
    public String getWidth() { return width; }
    public String getHeight() { return height; }
    public String getTitle() { return title; }
    // MUTATOR METHODS
    public void setSelectedSlide(ImageModel initSelectedSlide) {
	selectedSlide = initSelectedSlide;
    }
    public void setWidth(String initWidth) {
	width = initWidth;
    }
     public void setHeight(String initHeight) {
	height = initHeight;
    }
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     */
    public void addSlide() {
	ImageModel slideToAdd = new ImageModel(DEFAULT_IMAGE,PATH_IMAGES, DEFAULT_STRING, DEFAULT_STRING,
                                                DEFAULT_STRING,DEFAULT_STRING);
	slides.add(slideToAdd);
        setSelectedSlide(slideToAdd);
    }

    /**
     * Removes the currently selected slide from the slide show and
     * updates the display.
     */
    public void removeSelectedSlide() {
	if (isSlideSelected()) {
	    slides.remove(selectedSlide);
	    selectedSlide = null;
	}
    }
 
    /**
     * Moves the currently selected slide up in the slide
     * show by one slide.
     */
    public void moveSelectedSlideUp() {
	if (isSlideSelected()) {
	    moveSlideUp(selectedSlide);
	}
    }
    
    // HELPER METHOD
    private void moveSlideUp(ImageModel slideToMove) {
	int index = slides.indexOf(slideToMove);
	if (index > 0) {
	    ImageModel temp = slides.get(index);
	    slides.set(index, slides.get(index-1));
	    slides.set(index-1, temp);
	}
    }
    
    /**
     * Moves the currently selected slide down in the slide
     * show by one slide.
     */
    public void moveSelectedSlideDown() {
	if (isSlideSelected()) {
	    moveSlideDown(selectedSlide);
	}
    }
    
    // HELPER METHOD
    private void moveSlideDown(ImageModel slideToMove) {
	int index = slides.indexOf(slideToMove);
	if (index < (slides.size()-1)) {
	    ImageModel temp = slides.get(index);
	    slides.set(index, slides.get(index+1));
	    slides.set(index+1, temp);
	}
    }
    
    
}

