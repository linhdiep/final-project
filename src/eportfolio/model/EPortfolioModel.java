/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import eportfolio.view.EPortfolioEditorView;
import eportfolio.view.PageEditorView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Linh
 */
public class EPortfolioModel {

    EPortfolioEditorView ui;
    String studentName;
    String title;
    ObservableList<PageModel> pages;
    PageModel selectedPage;
    
    public EPortfolioModel(EPortfolioEditorView initUI, String initStudentName) {
        studentName = initStudentName;
	ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(PageModel testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<PageModel> getPages() {
	return pages;
    }
    
    public PageModel getSelectedPage() {
	return selectedPage;
    }

    public String getStudentName() { 
	return studentName; 
    }
     public String getTitle() { 
	return title; 
    }
     
    // MUTATOR METHODS
    public void setSelectedPage(PageModel initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    
    public void setStudentName(String initStudentName) { 
	studentName = initStudentName; 
    }
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	pages.clear();
	selectedPage = null;
    }

    /**
     * Adds a page to the portfolio with the parameter settings.
     **/
    public void addPage(String initTitle, String initFooter, String initLayout, String initColor,
                        String initFont, String initBannerFileName, String initBannerPath){
	PageModel pageToAdd = new PageModel(initTitle,initFooter,initLayout,initColor,
                                            initFont, initBannerFileName,initBannerPath);
	pages.add(pageToAdd);
        selectedPage = pageToAdd;
        ui.reloadPageListView();
	ui.reloadPageEditorWorkspace();
    }

    /**
     * Removes the currently selected page from the portfolio and
     * updates the display.
     */
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
            ui.reloadPageListView();
            ui.reloadPageEditorWorkspace();
	}
    }

    
}
