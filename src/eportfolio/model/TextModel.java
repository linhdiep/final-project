/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.model;

import static eportfolio.StartupConstants.TEXT_TYPE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

/**
 *
 * @author Linh
 */
public class TextModel  extends Component {
    Stage componentView;
    String textType;
    String fontFamily;
    String fontSize;
    String fontStyle;
    String content;
    ObservableList<ListPointModel> listContent;
    ListPointModel selectedListPoint;
     
    /**
     * Constructor, it initializes all slide data.
     */
    public TextModel(String initTextType, String initFontFamily, String initFontSize, String initFontStyle) {
	textType = initTextType;
        fontFamily = initFontFamily;
        fontSize = initFontSize;
        fontStyle = initFontStyle;
	listContent = FXCollections.observableArrayList();
    }
    
    // ACCESSOR METHODS
    public String getTextType() { return textType; }
    public String getFontFamily() { return fontFamily; }
    public String getFontStyle() { return fontStyle; }
    public String getFontSize() { return fontSize; }
    public String getContent() { return content; }
    public String getType(){
        return TEXT_TYPE;
    }
     public boolean isListPointSelected() {
	return selectedListPoint != null;
    }
    
    public boolean isSelectedListPoint(ListPointModel testPoint) {
	return selectedListPoint == testPoint;
    }
   
    public ListPointModel getSelectedListPoint() {
	return selectedListPoint;
    }
    public ObservableList<ListPointModel> getListContent(){ return listContent;};
   
    
    // MUTATOR METHODS
    public void setTextType(String initTextType) {
	textType = initTextType;
    }
    public void setFontFamily(String initFontFamily) {
	fontFamily = initFontFamily;
    }
    public void setFontStyle(String initFontStyle) {
	fontStyle = initFontStyle;
    }
    public void setFontSize(String initFontSize) {
	fontSize = initFontSize;
    }
    public void setContent(String initContent) {
	content = initContent;
    }
    public void setListContent(ObservableList<ListPointModel> initListContent) {
        listContent = initListContent;
    }
     public void setSelectedListPoint(ListPointModel initSelectedListPoint) {
	selectedListPoint = initSelectedListPoint;
    }
    // SERVICE METHODS
    public void addListPoint(ListPointModel point){
        listContent.add(point);
        setSelectedListPoint(point);
    }
      public void removeSelectedListPoint() {
	if (isListPointSelected()) {
	    listContent.remove(selectedListPoint);
	    selectedListPoint = null;
	}
    }
 
}
