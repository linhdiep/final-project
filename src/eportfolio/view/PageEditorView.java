
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_BANNER_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_COMPONENT;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_TEXT;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_CONTROL;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_EDIT;
import static eportfolio.StartupConstants.CSS_CLASS_EDITOR_WORKSPACE_SELECTED_COMPONENT;
import static eportfolio.StartupConstants.DEFAULT_BANNER_THUMBNAIL_WIDTH;
import eportfolio.controller.FileController;
import eportfolio.controller.PageController;
import eportfolio.error.ErrorHandler;
import eportfolio.model.Component;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.PageModel;
import static eportfolio.view.SiteViewer.SLASH;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Linh Diep
 */
public class PageEditorView extends HBox{
    HBox componentsControlWrapper, infoWrapper;
    VBox  componentsWrapper, info ;
    // TEXT PANE
    GridPane textPane;
    Label    student_name;
    Label    page_title;
    Label    footer_text;
    TextField student_name_txt, page_title_txt, footer_text_txt;
    
    // LAYOUT PANE
    GridPane layoutPane;
    Label    select_layout;
    Label    select_color;
    Label    select_font;
    ComboBox layoutComboBox, colorComboBox, fontComboBox;
       
    
    // BANNER PANE
    HBox bannerPane;
    Pane bannerView;
    Label   banner;
    Button   select_banner;
    ImageView bannerSelectionView;
    
    // COMPONENTS CONTROL PANE
    VBox    componentsControlPane;
    Label   components;
    Button add,remove,edit;
    
    
    // COMPONENTS PANE
    ScrollPane componentPane;
    VBox componentPaneList;
    
    // The ui, model and file we're working on
    EPortfolioEditorView ui;
    EPortfolioModel eportfolio;
    PageModel page;
    PageController pageController;
    
    public PageEditorView(EPortfolioEditorView initUI, PageModel initPage){
        //init 
        ui = initUI;
        eportfolio = ui.getEPortfolio();
        page = initPage;
        
        // set up
        initComponentsControl();
        
        initComponents();
        
        initInfoPane();
        
        initEventHandlers();
        
        
        // Put 3 biggest parts into body
        componentsControlWrapper = new HBox();
        componentsWrapper = new VBox();
        infoWrapper = new HBox();
        componentsControlWrapper.getChildren().add(componentsControlPane);
        componentsWrapper.getChildren().add(componentPane);
        infoWrapper.getChildren().add(info);
        this.getChildren().addAll(componentsControlWrapper,componentsWrapper,infoWrapper);
    }
    
    
    private void initInfoPane(){
        initTextPane();
        
        initLayoutPane();
        
        initBannerPane();
        
        info = new VBox();
        info.getChildren().addAll(textPane,layoutPane,bannerPane, bannerView);
        info.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_EDIT);
        
    }
    public void initTextPane() {
        student_name = new Label ("Student Name: ");
        page_title = new Label ("Page Title: ");
        footer_text = new Label ("Footer Text: ");
        student_name_txt = new TextField();
        page_title_txt = new TextField();
        footer_text_txt = new TextField();
        
        // CONTENT
        textPane = new GridPane();
        textPane.add(student_name, 0, 0); 
        textPane.add(student_name_txt, 1, 0); 
        textPane.add(page_title, 0, 1); 
        textPane.add(page_title_txt, 1, 1); 
        textPane.add(footer_text, 0, 2); 
        textPane.add(footer_text_txt, 1, 2); 
    }

    public void initLayoutPane() {
        select_layout = new Label ("Select Layout: ");
        select_color = new Label ("Select Template Color: ");
        select_font = new Label ("Select Font: ");
        
        // CREATE COMBO BOXES
        layoutComboBox = new ComboBox();
        colorComboBox = new ComboBox();
        fontComboBox = new ComboBox();
        
        layoutComboBox.getItems().addAll(
                "layout1",
                "layout2",
                "layout3",
                "layout4",
                "layout5"
        );
        
        colorComboBox.getItems().addAll(
                "black",
                "white",
                "blue",
                "green",
                "red"
        );
        
        fontComboBox.getItems().addAll(
                "'Slabo 27px', serif",
                "'Open Sans', sans-serif",
                "'Titillium Web', sans-serif",
                "'Pacifico', cursive",
                "'Lobster', cursive"
        );
        
        // CONTENT
        layoutPane = new GridPane();
        layoutPane.add(select_layout, 0, 0); 
        layoutPane.add(layoutComboBox, 1, 0); 
        layoutPane.add(select_color, 0, 1); 
        layoutPane.add(colorComboBox, 1, 1); 
        layoutPane.add(select_font, 0, 2); 
        layoutPane.add(fontComboBox, 1, 2); 
    }

    public void initBannerPane() {
        bannerPane = new HBox();
        select_banner = initChildButton(bannerPane , "Select Banner Image", CSS_CLASS_BANNER_BUTTON, false);
        bannerView = new Pane();
        bannerSelectionView = new ImageView();
        bannerView.getChildren().add(bannerSelectionView);
    }

    private void initComponentsControl() {
        components = new Label ("Components");
        componentsControlPane = new VBox();
        componentsControlPane.getChildren().add(components);
        // BUTTONS
        add = initChildButton(componentsControlPane , "Add", CSS_CLASS_COMPONENT_BUTTON, false);
        remove = initChildButton(componentsControlPane , "Remove", CSS_CLASS_COMPONENT_BUTTON, false);
        edit = initChildButton(componentsControlPane , "Edit", CSS_CLASS_COMPONENT_BUTTON, false);
      
        componentsControlPane.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_CONTROL);
    }

    private void initComponents() {
        componentPaneList = new VBox();
        componentPane = new ScrollPane(componentPaneList);
        componentPaneList.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_PANE);
        
    }
    
    public void initEventHandlers() {
	// INFO 
        student_name_txt.textProperty().addListener(e -> {
	    eportfolio.setStudentName(student_name_txt.getText());
	    ui.updateFileToolbarControls(false);
	});
        page_title_txt.textProperty().addListener(e -> {
	    page.setTitle(page_title_txt.getText());
	    ui.updateFileToolbarControls(false);
	});
        footer_text_txt.textProperty().addListener(e -> {
	    page.setFooter(footer_text_txt.getText());
	    ui.updateFileToolbarControls(false);
	});
        layoutComboBox.valueProperty().addListener(e -> {
	    page.setLayout(layoutComboBox.getValue().toString());
	    ui.updateFileToolbarControls(false);
	});
        
        fontComboBox.valueProperty().addListener(e -> {
	    page.setFont(fontComboBox.getValue().toString());
	    ui.updateFileToolbarControls(false);
	});
        colorComboBox.valueProperty().addListener(e -> {
	    page.setColor(colorComboBox.getValue().toString());
	    ui.updateFileToolbarControls(false);
	});
        
        pageController = new PageController(ui);
        // BANNER
        select_banner.setOnAction(e -> {
	    pageController.processSelectBanner(page, this);
	});
        
        //COMPONENTS CONTROL
	add.setOnAction(e -> {
	    pageController.processAddComponentRequest();
            updateComponentToolbarControls();
            reloadComponentPaneView();
	});
	remove.setOnAction(e -> {
	    pageController.processRemoveComponentRequest();
            updateComponentToolbarControls();
	});
        edit.setOnAction(e -> {
	    pageController.processEditComponentRequest();
	});
        
    }
    public Button initChildButton(
	    Pane toolbar, 
            String buttonName,
	    String cssClass,
	    boolean disabled) {
	Button button = new Button(buttonName);
        button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	toolbar.getChildren().add(button);
	return button;
    }

    public void updateBanner() {
        String imagePath = page.getBannerPath() + SLASH + page.getBannerFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image banner = new Image(fileURL.toExternalForm());
	    bannerSelectionView.setImage(banner);
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_BANNER_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / banner.getWidth();
	    double scaledHeight = banner.getHeight() * perc;
	    bannerSelectionView.setFitWidth(scaledWidth);
	    bannerSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("UPDATE BANNER ERROR");
	}
    }
    
    public void reloadComponentPaneView() {
        componentPaneList.getChildren().clear();
	for (Component component : page.getComponents()) {
	    Pane comp = new Pane();
            Label compText = new Label(component.getType());
            compText.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_COMPONENT_TEXT);
            comp.getChildren().add(compText);
            
	    if (page.isSelectedComponent(component)){
		comp.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_SELECTED_COMPONENT);
            }
            else{
		comp.getStyleClass().add(CSS_CLASS_EDITOR_WORKSPACE_COMPONENT);
            }
            componentPaneList.getChildren().add(comp); 
            comp.setOnMousePressed(e -> {
                    page.setSelectedComponent(component);
                    this.reloadComponentPaneView();
            });
	}
	ui.updatePageToolbarControls();
        updateComponentToolbarControls();
    }
    
    public void reloadInfo() {
        student_name_txt.setText(eportfolio.getStudentName());
        page_title_txt.setText(page.getTitle());
        footer_text_txt.setText(page.getFooter());
        layoutComboBox.getSelectionModel().select(page.getLayout());
        colorComboBox.getSelectionModel().select(page.getColor());
        fontComboBox.getSelectionModel().select(page.getFont());
        updateBanner();
    }
    public void updateComponentToolbarControls() {
	add.setDisable(false);
	boolean componentSelected = page.isComponentSelected();
	remove.setDisable(!componentSelected);
        edit.setDisable(!componentSelected);
    }
}
