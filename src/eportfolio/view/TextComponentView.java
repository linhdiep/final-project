/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG;
import static eportfolio.StartupConstants.CSS_CLASS_HYPERLINK_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_LIST_CONTROL;
import static eportfolio.StartupConstants.CSS_CLASS_LIST_POINT;
import static eportfolio.StartupConstants.CSS_CLASS_SAVE_COMP_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_SELECTED_LIST_POINT;
import static eportfolio.StartupConstants.HEADER_TYPE;
import static eportfolio.StartupConstants.LIST_TYPE;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import eportfolio.controller.ComponentController;
import eportfolio.model.Component;
import eportfolio.model.ListPointModel;
import eportfolio.model.TextModel;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author Linh Diep
 */
public class TextComponentView extends Stage {
    ComponentController compController;
    String textType;
    TextModel text;
    BorderPane textPane;
    GridPane settingPane, botPane;
    FlowPane workPane;
    ScrollPane listWorkPane,paraWorkPane;
    VBox listControlToolbar, listPointPane;
    HBox listPointWrap;
    Button hyperlink_button, save_button, add_list_button, remove_list_button;
    ComboBox fontComboBox, styleComboBox, sizeComboBox, sizeHeaderComboBox;
    TextField headerText, list_point;
    TextArea paraPane;
    Label fontLabel, styleLabel, sizeLabel;
    public TextComponentView(String initTextType, Component initText) {
        textType = initTextType;
        text = (TextModel)initText;
	// SET THE WINDOW TITLE
	this.setTitle(text.getType()+ " | " +text.getTextType());
        
        // SET UP THE COMBO BOXES FRO FONT, STYLE
	fontComboBox = new ComboBox();
        styleComboBox = new ComboBox();
        sizeComboBox = new ComboBox();
        sizeHeaderComboBox = new ComboBox();
        
        // CREATE COMBO BOX
        fontComboBox.getSelectionModel().select("Font");
	fontComboBox.getItems().addAll(
                "'Slabo 27px', serif",
                "'Open Sans', sans-serif",
                "'Titillium Web', sans-serif",
                "'Pacifico', cursive",
                "'Lobster', cursive"
        );
        
        styleComboBox.getSelectionModel().select("Style");
        styleComboBox.getItems().addAll(
                "normal",
                "oblique",
                "italic"
        );
        
        // COMBO BOX FOR SIZE
        sizeHeaderComboBox.getSelectionModel().select("Size");
        sizeHeaderComboBox.getItems().addAll(
                "h1","h2","h3","h4"
        );
        
        sizeComboBox.getSelectionModel().select("Size");
        sizeComboBox.getItems().addAll(
                "11","12","14","16","18","20","22","24"
        );
        
        // SET UP BUTTONS
        // for List
        add_list_button = new Button("Add");
        remove_list_button = new Button ("Remove");
        add_list_button.getStyleClass().add(CSS_CLASS_HYPERLINK_BUTTON);
        remove_list_button.getStyleClass().add(CSS_CLASS_HYPERLINK_BUTTON);
        // for hyperlink
        hyperlink_button = new Button("Add/Edit Hyperlink");
        hyperlink_button.getStyleClass().add(CSS_CLASS_HYPERLINK_BUTTON);
        // Save
        save_button = new Button("Save");
        save_button.getStyleClass().add(CSS_CLASS_SAVE_COMP_BUTTON);
        
        // SET UP LABELS
        fontLabel = new Label("Font: ");
        styleLabel = new Label ("Style: ");
        sizeLabel = new Label ("Size: ");
        
        // FOR TOP AND CENTER
        
        settingPane = new GridPane();
        settingPane.add(fontLabel,0,0);
        settingPane.add(styleLabel,1,0);
        settingPane.add(sizeLabel,2,0);
        settingPane.add(fontComboBox,0,1);
        settingPane.add(styleComboBox,1,1);
        settingPane.setHgap(20); 
        settingPane.setVgap(20); 
        settingPane.setPadding(new Insets(10, 10, 10, 10));
        
        // SET UP THE SIZE & OTHER FEATURES DEPENDING ON TEXT TYPE
        
        
        // FOR BOTTOM
        botPane = new GridPane();
        botPane.add(save_button,1,0);
        
        initEventHandlers();
        reloadInfo();
        // PUT EVERYTHING ON THE TEXTPANE
        textPane = new BorderPane();
        textPane.setTop(settingPane);
        textPane.setCenter(workPane);
        textPane.setBottom(botPane);
        textPane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG);
	// NOW SET THE SCENE IN THIS WINDOW
        
	Scene scene = new Scene(textPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        this.showAndWait();
    }
    
    public void initHeader(){
        // PUT TO SETTINGPANE
        settingPane.add(sizeHeaderComboBox,2,1);
         // CREATE WORKPANE
        workPane = new FlowPane();
        headerText = new TextField();
        headerText.textProperty().addListener(e -> {
	     text.setContent(headerText.getText());
	});
        text.setContent(headerText.getText());
        headerText.setPrefWidth(700);
        workPane.getChildren().add(headerText);
    }
    
    private void initList(){
        // PUT EVERYTHING TO SETTINGPANE
        settingPane.add(sizeComboBox,2,1);   
        
         // LIST CONTROL TOOLBAR
        listControlToolbar = new VBox();
        listControlToolbar.getChildren().addAll(add_list_button, remove_list_button);
        listControlToolbar.getStyleClass().add(CSS_CLASS_LIST_CONTROL);
        
        // SCROLLPANE CONTAINNING LIST
        listPointPane = new VBox();
        listWorkPane = new ScrollPane(listPointPane);
        
         // CREATE WORKPANE
        workPane = new FlowPane();
        workPane.getChildren().addAll(listControlToolbar,listWorkPane);
        //workPane.getStyleClass().add(CSS_CLASS_LIST_SCROLLBAR);
    }
    
    private void initPara() {
        // PUT EVERYTHING TO SETTINGPANE
        settingPane.add(sizeComboBox,2,1);   
        settingPane.add(hyperlink_button,3,0);
        // CREATE WORKPANE
        paraPane = new TextArea();
        paraWorkPane = new ScrollPane(paraPane);
        workPane = new FlowPane();
        workPane.getChildren().add(paraWorkPane);
        text.setContent(paraPane.getText());
    }
    
    public void initEventHandlers() {
        compController = new ComponentController();
	save_button.setOnAction(e -> {
	    text.setFontFamily(fontComboBox.getValue().toString());
            text.setFontStyle(styleComboBox.getValue().toString());
            if (textType.equals(HEADER_TYPE)){
                text.setFontSize(sizeHeaderComboBox.getValue().toString());
                
            }else{
                text.setFontSize(sizeComboBox.getValue().toString());
            } 
        });
        add_list_button.setOnAction(e -> {
            compController.processAddListPointRequest(this);
        });
            
        remove_list_button.setOnAction(e -> {
            compController.processRemoveListPointRequest(this);
        });
            
        hyperlink_button.setOnAction(e -> {
            processAddEditHyperlinkRequest();
        });
        
        
    }
    public void reloadListPane() {
	listPointPane.getChildren().clear();
	for (ListPointModel listPoint : text.getListContent()) {
            ListPointView list_point = new ListPointView(listPoint);
	    if (text.isSelectedListPoint(listPoint)){
		list_point.getStyleClass().add(CSS_CLASS_SELECTED_LIST_POINT);
            }
            else{
                list_point.getStyleClass().add(CSS_CLASS_LIST_POINT);
            }
            listPointPane.getChildren().add(list_point);
            list_point.setOnMousePressed(e -> {
                text.setSelectedListPoint(listPoint);
                this.reloadListPane();
            });
	}
            updateListEditToolbarControls();
        }
    
     public void reloadInfo() {
        fontComboBox.setValue(text.getFontFamily());
        styleComboBox.setValue(text.getFontStyle());
        if (textType.equals(HEADER_TYPE)){
            sizeHeaderComboBox.setValue(text.getFontSize());
            initHeader();
            headerText.setText(text.getContent());
        }else{
                sizeComboBox.setValue(text.getFontSize());
                if (textType.equals(LIST_TYPE)){
                    initList();
                    reloadListPane();
                }
                else{
                    initPara();
                    paraPane.setText(text.getContent());
                }
            } 
    }

    private void updateListEditToolbarControls() {
        add_list_button.setDisable(false);
	boolean listPointSelected = text.isListPointSelected();
	remove_list_button.setDisable(!listPointSelected);  
    }
    
    public TextModel getText(){
        return text;
    }

    private void processAddEditHyperlinkRequest() {
        String url = JOptionPane.showInputDialog(null,
            "Add/Edit Hyperlink",
            "Enter Hyperlink: ",
            JOptionPane.QUESTION_MESSAGE);
        String hyperlinkHtml = "<a href='" +url.trim() +"' title='" + paraPane.getSelectedText() +"'>" +  paraPane.getSelectedText() + "</a>";
        paraPane.replaceSelection(hyperlinkHtml);
        String content = paraPane.getText();
        text.setContent(content);
    }
}
 