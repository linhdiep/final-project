
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_CHOOSE_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG_INNER;
import static eportfolio.StartupConstants.CSS_CLASS_SAVE_COMP_BUTTON;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import eportfolio.controller.ComponentController;
import eportfolio.error.ErrorHandler;
import eportfolio.model.Component;
import eportfolio.model.VideoModel;
import static eportfolio.view.SiteViewer.SLASH;
import java.io.File;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 *
 * @author Linh Diep
 */
public class VideoComponentView extends Stage {
    VideoModel video;
    HBox videoPane;
    GridPane widthHeightPane;
    VBox videosettingPane;
    MediaView videoView;
    Button video_button, save_button;
    TextField width,height, caption;
    Label width_label,height_label, caption_label;
    ComponentController compController;
    MediaPlayer mediaPlayer;
    public VideoComponentView(Component initVideo){
        video = (VideoModel)initVideo;
        
        this.setTitle(video.getType());
       
        //WIDTH HEIGHT PANE
        caption_label = new Label("Caption: ");
        width_label = new Label("Width: ");
        height_label = new Label("Height: ");
        
        caption = new TextField();
        width = new TextField();
        height = new TextField();
        
        caption.setMaxWidth(150);
        width.setMaxWidth(100);
        height.setMaxWidth(100);
        
        widthHeightPane = new GridPane();
        widthHeightPane.add(width_label, 0, 0); 
        widthHeightPane.add(width, 1, 0); 
        widthHeightPane.add(height_label, 0, 1); 
        widthHeightPane.add(height, 1, 1); 
        widthHeightPane.add(caption_label, 0, 2); 
        widthHeightPane.add(caption, 1, 2); 
        widthHeightPane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG_INNER);
        
         // SETTING PANE
        video_button = new Button("Select Video");
        video_button.getStyleClass().add(CSS_CLASS_CHOOSE_BUTTON);
        save_button = new Button("Save");
        save_button.getStyleClass().add(CSS_CLASS_SAVE_COMP_BUTTON);
        
        videosettingPane = new VBox();
        videosettingPane.getChildren().add(video_button);
        videosettingPane.getChildren().add(widthHeightPane);
        videosettingPane.getChildren().add(save_button);
        videosettingPane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG);
        
        // MEDIA VIEW
        // Create the view and add it to the Scene.
	videoView = new MediaView();
        
        // PUT EVERYTHING INTO VIDEO PANE
        videoPane = new HBox();
        videoPane.getChildren().add(videosettingPane);
        videoPane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG);
        // NOW SET THE SCENE IN THIS WINDOW
        reloadInfo();
        initEventHandlers();
	Scene scene = new Scene(videoPane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        this.showAndWait();
    }
    
    public void updateVideo() {
        try {
            String videoPath = video.getVideoPath() + SLASH + video.getVideoFileName();
            File file = new File(videoPath);
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Media myVideo = new Media(fileURL.toExternalForm());
            
            // Create the player
            mediaPlayer = new MediaPlayer(myVideo);
            mediaPlayer.setAutoPlay(false);
            videoView.setMediaPlayer(mediaPlayer);
            // AND RESIZE IT
            videoView.setFitWidth(350);
            videoView.setFitHeight(250);
           
        videoPane.getChildren().add(videoView);
        } catch (Exception ex) {
            ErrorHandler eH = new ErrorHandler(null);
            eH.processError("UPDATE VIDEO ERROR");
        }
    }
    
    public void initEventHandlers() {
        compController = new ComponentController();
        video_button.setOnAction(e -> {
	    compController.processSelectVideo(video, this);
	});
	save_button.setOnAction(e -> {
	    video.setCaption(caption.getText());
            video.setWidth(width.getText());
            video.setHeight(height.getText());
        });
    }
    
     public void reloadInfo() {
        caption.setText(video.getCaption());
        width.setText(video.getWidth());
        height.setText(video.getHeight());
        updateVideo();
    }
}
