/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_LIST_POINT;
import eportfolio.model.ListPointModel;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author Linh
 */

public class ListPointView extends HBox{
TextField listPointTextField;
Label signal;
ListPointModel listPoint;
    public ListPointView(ListPointModel initListPoint){
        listPoint = initListPoint;
        signal = new Label(" + ");
        listPointTextField = new TextField();
        listPointTextField.setText(listPoint.getListPoint());
        listPointTextField.setPrefWidth(250);
        this.getChildren().addAll(signal, listPointTextField);
        listPointTextField.textProperty().addListener(e -> {
	    String text = listPointTextField.getText();
            listPoint.setListPoint(text);
	});
    }
}