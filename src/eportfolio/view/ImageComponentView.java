
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_CHOOSE_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG_INNER;
import static eportfolio.StartupConstants.CSS_CLASS_SAVE_COMP_BUTTON;
import static eportfolio.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import eportfolio.controller.ComponentController;
import eportfolio.error.ErrorHandler;
import eportfolio.model.Component;
import eportfolio.model.ImageModel;
import static eportfolio.view.SiteViewer.SLASH;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Linh Diep
 */
public class ImageComponentView extends Stage {
    ComponentController compController;
    ImageModel image;
    HBox imagePane;
    GridPane settingPane;
    VBox settingPaneSave;
    ImageView imageSelectionView;
    Button img_button, save_button;
    TextField width,height, caption;
    Label width_label,height_label, caption_label;
    ComboBox position;
    public ImageComponentView(Component initImage){
        image = (ImageModel)initImage;
        
        this.setTitle(image.getType());
       
        //SETTING PANE
        caption_label = new Label("Caption: ");
        width_label = new Label("Width: ");
        height_label = new Label("Height: ");
        
        caption = new TextField();
        width = new TextField();
        height = new TextField();
        position = new ComboBox();
        
        caption.setMaxWidth(150);
        width.setMaxWidth(100);
        height.setMaxWidth(100);
        
        // CREATE COMBO BOX
        position.getSelectionModel().select("Position");
	position.getItems().addAll(
                "Left",
                "Right"
        );
        
        settingPane = new GridPane();
        settingPane.add(width_label, 0, 0); 
        settingPane.add(width, 1, 0); 
        settingPane.add(height_label, 0, 1); 
        settingPane.add(height, 1, 1); 
        settingPane.add(caption_label, 0, 2); 
        settingPane.add(caption, 1, 2); 
        
        
         // SETTING PANE INCLUDING SAVE AND CHOOSE BUTTON
        img_button = new Button("Select Image");
        img_button.getStyleClass().add(CSS_CLASS_CHOOSE_BUTTON);
        save_button = new Button("Save");
        save_button.getStyleClass().add(CSS_CLASS_SAVE_COMP_BUTTON);
        
        settingPaneSave = new VBox();
        settingPaneSave.getChildren().add(img_button);
        settingPaneSave.getChildren().add(settingPane);
        settingPaneSave.getChildren().add(position);
        settingPaneSave.getChildren().add(save_button);
        settingPaneSave.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG_INNER);
        
        //IMG DISPLAY PANE
       // imageSelectionView = new ImageView();
        
        // PUT EVERYTHING INTO IMAGE PANE
        imagePane = new HBox();
        imagePane.getChildren().add(settingPaneSave);
        imagePane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG);
        
        reloadInfo();
        initEventHandlers();
        // NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(imagePane);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        this.showAndWait();
    }
    
    public void updateImage() {
        try {
            String imagePath = image.getImagePath() + SLASH + image.getImageFileName();
            File file = new File(imagePath);
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image img = new Image(fileURL.toExternalForm());
            imageSelectionView = new ImageView();
            imageSelectionView.setImage(img);
            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / img.getWidth();
            double scaledHeight = img.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
            imagePane.getChildren().add(imageSelectionView);
        } catch (MalformedURLException ex) {
            ErrorHandler eH = new ErrorHandler(null);
            eH.processError("UPDATE IMG ERROR");
        }
        
    }
    
    public void initEventHandlers() {
        compController = new ComponentController();
        img_button.setOnAction(e -> {
	    compController.processSelectImage(image, this);
	});
	save_button.setOnAction(e -> {
	    image.setCaption(caption.getText());
            image.setWidth(width.getText());
            image.setHeight(height.getText());
            image.setPosition(position.getValue().toString());
        });
    }
    
     public void reloadInfo() {
        caption.setText(image.getCaption());
        width.setText(image.getWidth());
        height.setText(image.getHeight());
        position.setValue(image.getPosition());
        updateImage();
    }
}
