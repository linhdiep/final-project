
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_CAPTION_PROMPT;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static eportfolio.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static eportfolio.StartupConstants.PATH_IMAGES;
import eportfolio.controller.ComponentController;
import eportfolio.error.ErrorHandler;
import eportfolio.model.ImageModel;
import static eportfolio.view.SiteViewer.SLASH;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author Linh Diep
 */
public class SlideEditView extends HBox {
    SlideshowComponentView ui;
    // SLIDE THIS COMPONENT EDITS
    ImageModel slide;
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ComponentController compController;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     */
    public SlideEditView(SlideshowComponentView initUi, ImageModel initSlide){
	// KEEP THIS FOR LATER
	ui = initUi;
	slide = initSlide;
        
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	captionLabel = new Label("Caption: ");
        captionLabel.getStyleClass().add(CSS_CLASS_CAPTION_PROMPT);
	captionTextField = new TextField();
        captionTextField.setText(slide.getCaption());
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
        

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        
        
        // EVENT HANDLER
        compController = new ComponentController();
        imageSelectionView.setOnMousePressed(e -> {
	  compController.processSelectSlide(slide, this);
	});
         
	captionTextField.textProperty().addListener(e -> {
	    String text = captionTextField.getText();
            slide.setCaption(text);
	});
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    ErrorHandler eH = new ErrorHandler(null);
            eH.processError("SLIDES ERROR");
	}
    }    
}