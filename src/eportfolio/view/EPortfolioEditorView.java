
package eportfolio.view;
import static eportfolio.StartupConstants.CSS_CLASS_FILE_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_FILE_TOOLBAR_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_MODE_TOOLBAR_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_MODE_TOOLBAR_TAB;
import static eportfolio.StartupConstants.CSS_CLASS_PAGE_LIST_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_PAGE_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_PAGE_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_PAGE_TOOLBAR_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_SELECTED_PAGE_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_WORKSPACE;
import static eportfolio.StartupConstants.DEFAULT_STRING;
import static eportfolio.StartupConstants.ICON_ADD_SITE;
import static eportfolio.StartupConstants.ICON_EXIT;
import static eportfolio.StartupConstants.ICON_EXPORT;
import static eportfolio.StartupConstants.ICON_LOAD;
import static eportfolio.StartupConstants.ICON_NEW;
import static eportfolio.StartupConstants.ICON_REMOVE_SITE;
import static eportfolio.StartupConstants.ICON_SAVE;
import static eportfolio.StartupConstants.ICON_SAVE_AS;
import static eportfolio.StartupConstants.PATH_ICONS;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import eportfolio.controller.FileController;
import eportfolio.controller.PageController;
import eportfolio.error.ErrorHandler;
import eportfolio.file.FileManager;
import eportfolio.file.SiteExporter;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.PageModel;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 *
 * @author Linh Diep
 */
public class EPortfolioEditorView {
    
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    int numOfPage = 1;
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ePane;
    // WORKSPACE
    BorderPane workspace;
    // THIS PANE CONTAINS 2 PANES IN THE LEFT
    HBox sidePane;
    
    // FILE TOOLBAR 
    FlowPane fileToolbarPane;
    Button newButton;
    Button loadButton;
    Button saveButton;
    Button saveAsButton;
    Button exportButton;
    Button exitButton;

    // PAGE TOOLBAR
    VBox pageToolbarPane;
    Button addPageButton;
    Button removePageButton;
    
    // PAGE LIST TOOLBAR
    VBox pageListPane;
    BorderPane pagePane;
    Label pageTitle;
    
    // WORKSPACE MODE TOOLBAR
    TabPane modeToolbarPane ;
    Tab pageEditorTab; 
    Tab siteViewerTab; 
   
    // SITE VIEWER WORKSPACE
    ScrollPane scrollPane;
    WebView webView;
    WebEngine webEngine;
    
    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    
    // THE EPORTFOLIO WE'RE WORKING WITH
    EPortfolioModel ePortfolio;
    
    // FOR VIEWING PAGE EDITOR
    PageEditorView pageEditor;
    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    FileManager fileManager;
    
    // THIS IS FOR EXPORTING THE SLIDESHOW SITE
    SiteExporter siteExporter;
    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO PAGE CONTROL BUTTONS
    private PageController pageController;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;


    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public EPortfolioEditorView(FileManager initFileManager,
				SiteExporter initSiteExporter) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// AND THE SITE EXPORTER
	siteExporter = initSiteExporter;
	
	// MAKE THE DATA MANAGING MODEL
	ePortfolio = new EPortfolioModel(this, DEFAULT_STRING);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }
    
    // ACCESSOR METHODS
    public EPortfolioModel getEPortfolio() {
	return ePortfolio;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE FILE TOOLBAR AT THE TOP
	initFileToolbar();
        
        // THE WORKSPACE MODE TOOLBAR RIGHT BELOW THE FILE TOOLBAR
	
        initModeToolbar();
        // THE SITE TOOLBAR AT THE MOST LEFT 
	initPageToolbar();

        // THE TOOLBAR IN THE RIGHT OF SITE TOOLBAR TO SHOW ALL THE PAGES AVAILABLE 
	initPageListView();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();
        
	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
	fileToolbarPane.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	newButton = initChildButton(fileToolbarPane, ICON_NEW,	"New",	    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
	loadButton = initChildButton(fileToolbarPane, ICON_LOAD,	"Load",    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
	saveButton = initChildButton(fileToolbarPane, ICON_SAVE,	"Save",    CSS_CLASS_FILE_TOOLBAR_BUTTON, true);
	saveAsButton = initChildButton(fileToolbarPane, ICON_SAVE_AS,	"Save As",    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
	exportButton = initChildButton(fileToolbarPane, ICON_EXPORT,	"Export",    CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, "Exit", CSS_CLASS_FILE_TOOLBAR_BUTTON, false);
    }
    
    private void initModeToolbar() {
            modeToolbarPane = new TabPane();
            modeToolbarPane.getStyleClass().add(CSS_CLASS_MODE_TOOLBAR_PANE);
            
            // CREATE PAGE EDITOR TAB
            pageEditorTab = new Tab();
            pageEditorTab.setText("Page Editor");
            pageEditorTab.getStyleClass().add(CSS_CLASS_MODE_TOOLBAR_TAB);
            pageEditorTab.setClosable(false);
            
            // CREATE SITE VIEWER TAB
            siteViewerTab = new Tab();
            siteViewerTab.setText("Site Viewer");
           
            siteViewerTab.getStyleClass().add(CSS_CLASS_MODE_TOOLBAR_TAB);
            siteViewerTab.setClosable(false);
            modeToolbarPane.getTabs().addAll(pageEditorTab,siteViewerTab);
    }
    
    private void initPageToolbar() {
        pageToolbarPane = new VBox();
        pageToolbarPane.getStyleClass().add(CSS_CLASS_PAGE_TOOLBAR_PANE);
	addPageButton = this.initChildButton(pageToolbarPane,	ICON_ADD_SITE,	    "Add Page",	    CSS_CLASS_PAGE_TOOLBAR_BUTTON,  true);
	removePageButton = this.initChildButton(pageToolbarPane, ICON_REMOVE_SITE,  "Remove Page",   CSS_CLASS_PAGE_TOOLBAR_BUTTON,  true);	
    }

    private void initPageListView() {
	pageListPane = new VBox();
        pageListPane.getStyleClass().add(CSS_CLASS_PAGE_LIST_PANE);
    }


    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());
        
        //SET UP LEFT SIDEBAR
        sidePane = new HBox();
        sidePane.getChildren().add(pageToolbarPane);
        sidePane.getChildren().add(pageListPane);
        
        // SET UP CENTER
        workspace = new BorderPane();
        workspace.setTop(modeToolbarPane);
        workspace.setCenter(pageEditor);
	
        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ePane = new BorderPane();
	ePane.getStyleClass().add(CSS_CLASS_WORKSPACE);
	ePane.setTop(fileToolbarPane);	
        ePane.setLeft(sidePane);
        
	primaryScene = new Scene(ePane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }

    private void initEventHandlers() {
       // FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager, siteExporter);
	newButton.setOnAction(e -> {
	    fileController.handleNewRequest();
	});
	loadButton.setOnAction(e -> {
	    fileController.handleLoadRequest();
	});
	saveButton.setOnAction(e -> {
            ePortfolio.setTitle(ePortfolio.getStudentName());
	    fileController.handleSaveRequest();
	});
        saveAsButton.setOnAction(e -> {
            ePortfolio.setTitle(saveAsDialog());
	    fileController.handleSaveRequest();
	});
	exportButton.setOnAction(e -> {
	    fileController.handleExportRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
        // MODE CONTROLS]
       siteViewerTab.setOnSelectionChanged(e -> {
            try {
                SiteViewer site = new SiteViewer(this, fileManager, siteExporter);
                siteViewerTab.setContent(site);
            } catch (MalformedURLException ex) {
                Logger.getLogger(EPortfolioEditorView.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
	// THEN THE PAGE CONTROLS
	pageController = new PageController(this);
	addPageButton.setOnAction(e -> {
	    pageController.processAddPageRequest(numOfPage);
            numOfPage++;
	});
	removePageButton.setOnAction(e -> {
	    pageController.processRemovePageRequest();
	});
    }
    public String saveAsDialog(){
        String response = JOptionPane.showInputDialog(null,
        "Save EPortfolio as...",
        "Enter EPortfolio's name: ",
        JOptionPane.QUESTION_MESSAGE);
        if(null == response){
            return ePortfolio.getStudentName();
        }
        else{
            return response;
        }
        
    }
    public void updateFileToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        ePane.setCenter(workspace);
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveButton.setDisable(saved);
	updatePageToolbarControls();
        reloadPageListView();
    }
    
    public void updatePageToolbarControls() {
	// AND THE PAGE TOOLBAR
	addPageButton.setDisable(false);
	boolean pageSelected = ePortfolio.isPageSelected();
	removePageButton.setDisable(!pageSelected);
    }
    public void reloadPageListView() {
        pageListPane.getChildren().clear();
	for (PageModel page : ePortfolio.getPages()) {
	    pagePane = new BorderPane();
            pageTitle = new Label(page.getTitle());
            pagePane.setCenter(pageTitle);
	    if (ePortfolio.isSelectedPage(page)){
		pagePane.getStyleClass().add(CSS_CLASS_SELECTED_PAGE_PANE);
            }
            else{
		pagePane.getStyleClass().add(CSS_CLASS_PAGE_PANE);
            }
	    pageListPane.getChildren().add(pagePane);
	    pagePane.setOnMousePressed(e -> {
		ePortfolio.setSelectedPage(page);
                this.reloadPageListView();
		this.reloadPageEditorWorkspace();
	    });
	}
	updatePageToolbarControls();
    }
    
    public void reloadPageEditorWorkspace() {
        PageEditorView pageEditor = new PageEditorView(this, ePortfolio.getSelectedPage());
	pageEditorTab.setContent(pageEditor); 
	updatePageToolbarControls();
        reloadPageListView();
        pageEditor.reloadInfo();
        pageEditor.reloadComponentPaneView();
    }
}


