
package eportfolio.view;

import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG;
import static eportfolio.StartupConstants.CSS_CLASS_COMPONENT_DIALOG_INNER;
import static eportfolio.StartupConstants.CSS_CLASS_SAVE_COMP_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDESHOW_TOOLBAR_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDES_EDITOR_PANE;
import static eportfolio.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static eportfolio.StartupConstants.ICON_ADD_SLIDE;
import static eportfolio.StartupConstants.ICON_MOVE_DOWN;
import static eportfolio.StartupConstants.ICON_MOVE_UP;
import static eportfolio.StartupConstants.ICON_REMOVE_SLIDE;
import static eportfolio.StartupConstants.PATH_ICONS;
import static eportfolio.StartupConstants.STYLE_SHEET_UI;
import eportfolio.controller.ComponentController;
import eportfolio.model.Component;
import eportfolio.model.ImageModel;
import eportfolio.model.SlideshowModel;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Linh Diep
 */
public class SlideshowComponentView extends Stage{
    SlideshowModel slideshow;
    ComponentController compController;
    GridPane widthHeightPane;
    VBox settingPane;
    Button save_button;
    TextField width,height,title;
    Label title_label,width_label,height_label;

    // WORKSPACE
    BorderPane workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
   
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;
    
    public SlideshowComponentView(Component initSlideshow){
        slideshow = (SlideshowModel)initSlideshow;
        
        this.setTitle(slideshow.getType());
        
        initSetting();
        initSlideshow();
        initEventHandlers();
        reloadInfo();
        reloadSlideShowPane();
        
        Scene scene = new Scene(workspace);
	scene.getStylesheets().add(STYLE_SHEET_UI);
	setScene(scene);
        this.showAndWait();
       
    }
    
    public SlideshowModel getSlideshow(){
        return slideshow;
    }
    private void initSetting(){
        title_label = new Label("Title: ");
        width_label = new Label("Width: ");
        height_label = new Label("Height: ");
        
        title = new TextField();
        width = new TextField();
        height = new TextField();
        width.setMaxWidth(100);
        height.setMaxWidth(100);
        
        widthHeightPane = new GridPane();
        widthHeightPane.add(title_label, 0, 0); 
        widthHeightPane.add(title, 1, 0); 
        widthHeightPane.add(width_label, 0, 1); 
        widthHeightPane.add(width, 1, 1); 
        widthHeightPane.add(height_label, 0, 2); 
        widthHeightPane.add(height, 1, 2); 
        
        save_button = new Button("Save");
        save_button.getStyleClass().add(CSS_CLASS_SAVE_COMP_BUTTON);
        settingPane = new VBox();
        settingPane.getChildren().add(widthHeightPane);
        settingPane.getChildren().add(save_button);
        settingPane.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG_INNER);
    }
   
    public void initSlideshow(){
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    "Add Slide",	    CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON,  true);
	removeSlideButton = this.initChildButton(slideEditToolbar,	ICON_REMOVE_SLIDE,  "Remove Slide",   CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON,  true);
	moveSlideUpButton = this.initChildButton(slideEditToolbar,	ICON_MOVE_UP,	    "Move Up",	    CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON,  true);
	moveSlideDownButton = this.initChildButton(slideEditToolbar,	ICON_MOVE_DOWN,	    "Move Down",	    CSS_CLASS_SLIDESHOW_TOOLBAR_BUTTON,  true);
	
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
	slidesEditorScrollPane.setFitToWidth(true);
	slidesEditorScrollPane.setFitToHeight(true);
	

	// SETUP ALL THE STYLE CLASSES
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDESHOW_TOOLBAR_PANE);
	slidesEditorPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
	slidesEditorScrollPane.getStyleClass().add(CSS_CLASS_SLIDES_EDITOR_PANE);
        
        // NOW PUT THESE TWO IN THE WORKSPACE
        
        workspace = new BorderPane();
	workspace.setLeft(slideEditToolbar);
	workspace.setCenter(slidesEditorScrollPane);
        workspace.setRight(settingPane);
        workspace.getStyleClass().add(CSS_CLASS_COMPONENT_DIALOG);
    }
    
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    String tooltip, 
	    String cssClass,
	    boolean disabled) {
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
      private void initEventHandlers() {
	// THEN THE SLIDE SHOW EDIT CONTROLS
	compController = new ComponentController();
	addSlideButton.setOnAction(e -> {
	    compController.processAddSlideRequest(this);
	});
	removeSlideButton.setOnAction(e -> {
	    compController.processRemoveSlideRequest(this);
	});
	moveSlideUpButton.setOnAction(e -> {
	    compController.processMoveSlideUpRequest(this);
	});
	moveSlideDownButton.setOnAction(e -> {
	    compController.processMoveSlideDownRequest(this);
	});
        
        save_button.setOnAction(e -> {
	    slideshow.setTitle(title.getText());
            slideshow.setWidth(width.getText());
            slideshow.setHeight(height.getText());
        });
    }
      public void updateSlideshowEditToolbarControls() {
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
	boolean slideSelected = slideshow.isSlideSelected();
	removeSlideButton.setDisable(!slideSelected);
	moveSlideUpButton.setDisable(!slideSelected);
	moveSlideDownButton.setDisable(!slideSelected);	
    }
      /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     */
    public void reloadSlideShowPane() {
	slidesEditorPane.getChildren().clear();
	for (ImageModel slide : slideshow.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(this, slide);
	    if (slideshow.isSelectedSlide(slide)){
		slideEditor.getStyleClass().add(CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW);
            }
            else{
		slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
            }
	    slidesEditorPane.getChildren().add(slideEditor);
	    slideEditor.setOnMousePressed(e -> {
		slideshow.setSelectedSlide(slide);
		this.reloadSlideShowPane();
	    });
	}
	updateSlideshowEditToolbarControls();
    }
    
    public void reloadInfo() {
        title.setText(slideshow.getTitle());
        width.setText(slideshow.getWidth());
        height.setText(slideshow.getHeight());
    }
}
