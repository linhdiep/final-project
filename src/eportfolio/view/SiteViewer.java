
package eportfolio.view;


import eportfolio.controller.FileController;
import eportfolio.file.FileManager;
import eportfolio.file.SiteExporter;
import eportfolio.model.EPortfolioModel;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * This class provides the UI for the slide show viewer, note that this class is
 * a window and contains all controls inside.
 *
 * @author Linh Diep
 */
public class SiteViewer extends Pane {

    // THE MAIN UI

    EPortfolioEditorView parentView;

    // THE DATA 
    EPortfolioModel ePort;
    FileController fileController;
    FileManager fileManager;
    SiteExporter siteExporter;
    // HERE ARE OUR UI CONTROLS
    ScrollPane scrollPane;
    WebView webView;
    WebEngine webEngine;
    public static String SLASH = "/";
    public static String SITES_DIR = "./sites/"; 
    public static String HTML_EXT = ".html";
    /**
     * This constructor just initializes the parent and slides references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public SiteViewer(EPortfolioEditorView initParentView, FileManager initFileManager, SiteExporter initSiteExporter) throws MalformedURLException {
	// KEEP THIS FOR LATER
	parentView = initParentView;
        ePort = parentView.getEPortfolio();
        fileManager = initFileManager;
        siteExporter = initSiteExporter;
        fileController = new FileController(parentView, fileManager, siteExporter);
        fileController.handleSaveRequest();
        fileController.handleExportRequest();
	// GET THE PAGES
        // SETUP THE UI
	webView = new WebView();
	scrollPane = new ScrollPane(webView);
	// GET THE URL
        String indexPath = SITES_DIR + ePort.getTitle() + SLASH + ePort.getSelectedPage().getNum()+ HTML_EXT;
	File indexFile = new File(indexPath);
	URL indexURL = indexFile.toURI().toURL();
	
	// SETUP THE WEB ENGINE AND LOAD THE URL
	webEngine = webView.getEngine();
	webEngine.load(indexURL.toExternalForm());
	webEngine.setJavaScriptEnabled(true);
	// NOW PUT STUFF IN THE PANE
        this.getChildren().add(webView);
    }

}


