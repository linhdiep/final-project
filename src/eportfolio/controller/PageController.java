/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.controller;

import static eportfolio.StartupConstants.DEFAULT_IMAGE;
import static eportfolio.StartupConstants.DEFAULT_STRING;
import static eportfolio.StartupConstants.HEADER_TYPE;
import static eportfolio.StartupConstants.IMAGE_TYPE;
import static eportfolio.StartupConstants.INPUT_TYPE;
import static eportfolio.StartupConstants.LABEL_TEXT_TYPE_SELECTION_PROMPT;
import static eportfolio.StartupConstants.LABEL_TYPE_SELECTION_PROMPT;
import static eportfolio.StartupConstants.LIST_TYPE;
import static eportfolio.StartupConstants.PARA_TYPE;
import static eportfolio.StartupConstants.PATH_IMAGES;
import static eportfolio.StartupConstants.SLIDESHOW_TYPE;
import static eportfolio.StartupConstants.TEXT_TYPE;
import static eportfolio.StartupConstants.VIDEO_TYPE;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.PageModel;
import eportfolio.model.TextModel;
import eportfolio.view.EPortfolioEditorView;
import eportfolio.view.ImageComponentView;
import eportfolio.view.PageEditorView;
import eportfolio.view.SlideshowComponentView;
import eportfolio.view.TextComponentView;
import eportfolio.view.VideoComponentView;
import java.io.File;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Linh
 */
public class PageController {
     // APP UI
    private EPortfolioEditorView ui;
    
    // COMPONENTS CONTROL
    
    String selectedType;
    String selectedTextType;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PageController(EPortfolioEditorView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddPageRequest(int numOfPage) {
	EPortfolioModel eportfolio = ui.getEPortfolio();
        eportfolio.addPage(DEFAULT_STRING, DEFAULT_STRING, DEFAULT_STRING,DEFAULT_STRING, 
                            DEFAULT_STRING,DEFAULT_IMAGE,PATH_IMAGES);
        PageModel page = eportfolio.getSelectedPage();
        page.setNum(numOfPage);
	ui.reloadPageListView();
        ui.reloadPageEditorWorkspace();
	ui.updateFileToolbarControls(false);
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemovePageRequest() {
	EPortfolioModel eportfolio = ui.getEPortfolio();
	eportfolio.removeSelectedPage();
	ui.reloadPageListView();
        ui.reloadPageEditorWorkspace();
	ui.updateFileToolbarControls(false);
    }
    
    public void processAddComponentRequest(){
       PageModel page = ui.getEPortfolio().getSelectedPage();
       selectedType = typeDialog();   
        if (selectedType.equals("0")){
            //Do nothing
        }
        else if(selectedType.equals(TEXT_TYPE)){ //TEXT
            selectedTextType = textTypeDialog();
            if (selectedTextType.equals("0")){
                selectedType = typeDialog();
            }
            if (selectedTextType.equals(HEADER_TYPE)){ //header 
                page.addComponent(TEXT_TYPE,HEADER_TYPE);   
                TextComponentView headerView = new TextComponentView(HEADER_TYPE,page.getSelectedComponent());
            }
            else if(selectedTextType.equals(LIST_TYPE)){ //list
                page.addComponent(TEXT_TYPE,LIST_TYPE);
                TextComponentView listView = new TextComponentView(LIST_TYPE,page.getSelectedComponent());
                
            }
            else{ //paragraph
                page.addComponent(TEXT_TYPE,PARA_TYPE);
                TextComponentView paraView = new TextComponentView(PARA_TYPE,page.getSelectedComponent());
            }
            
        }
        else if (selectedType.equals(IMAGE_TYPE)){ // IMAGE
            page.addComponent(IMAGE_TYPE,"");
            ImageComponentView imageView = new ImageComponentView(page.getSelectedComponent());
        }
        else if (selectedType.equals(VIDEO_TYPE)){ //VIDEO
            page.addComponent(VIDEO_TYPE,"");
            VideoComponentView videoView = new VideoComponentView(page.getSelectedComponent());
        }
        else { //SLIDESHOW 
            page.addComponent(SLIDESHOW_TYPE,"");
            SlideshowComponentView slideshowView = new SlideshowComponentView(page.getSelectedComponent());
        }
        ui.updateFileToolbarControls(false);
    }
    
    public void processRemoveComponentRequest(){
        PageModel page = ui.getEPortfolio().getSelectedPage();
	page.removeSelectedComponent();
        ui.reloadPageEditorWorkspace();
    }
    
    public void processEditComponentRequest(){
        PageModel page = ui.getEPortfolio().getSelectedPage();
        String type = page.getSelectedComponent().getType();
        if (type.equals(TEXT_TYPE)){ //TEXT
            TextModel textModel = (TextModel)page.getSelectedComponent();
            String subType = textModel.getTextType();
            TextComponentView textView = new TextComponentView(subType,page.getSelectedComponent());
        }
        else if (type.equals(IMAGE_TYPE)){ // IMAGE
            ImageComponentView imageView = new ImageComponentView(page.getSelectedComponent());
        }
        else if (type.equals(VIDEO_TYPE)){ //VIDEO
            VideoComponentView videoView = new VideoComponentView(page.getSelectedComponent());
        }
        else { //SLIDESHOW 
            SlideshowComponentView slideshowView = new SlideshowComponentView(page.getSelectedComponent());
        }
        ui.updateFileToolbarControls(false);
    }
    
    public String typeDialog(){
        Object[] type = {TEXT_TYPE, VIDEO_TYPE, SLIDESHOW_TYPE, IMAGE_TYPE};
        Object selectType = JOptionPane.showInputDialog(null,
            LABEL_TYPE_SELECTION_PROMPT, INPUT_TYPE,
            JOptionPane.INFORMATION_MESSAGE, null,
            type, type[0]);
        if(null == selectType){
            return "0";
        }
        else{
             return selectType.toString();
        }
    }
    
    public String textTypeDialog(){
        Object[] type = {PARA_TYPE, HEADER_TYPE, LIST_TYPE};
        Object selectTextType = JOptionPane.showInputDialog(null,
            LABEL_TEXT_TYPE_SELECTION_PROMPT, INPUT_TYPE,
            JOptionPane.INFORMATION_MESSAGE, null,
            type, type[0]);
        if(null == selectTextType){
             return "0";
        }
        else{
             return selectTextType.toString();
        }
           
    }
    
    public void processSelectBanner(PageModel pageToEdit, PageEditorView view) {
	FileChooser imageFileChooser = new FileChooser();
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    pageToEdit.setBanner(path, fileName);
	    view.updateBanner();
	    ui.updateFileToolbarControls(false);
	}
    }
}
