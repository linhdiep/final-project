/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfolio.controller;

import static eportfolio.StartupConstants.DEFAULT_IMAGE;
import eportfolio.error.ErrorHandler;
import eportfolio.model.EPortfolioModel;
import eportfolio.model.ImageModel;
import eportfolio.model.ListPointModel;
import eportfolio.model.SlideshowModel;
import eportfolio.model.TextModel;
import eportfolio.model.VideoModel;
import eportfolio.view.EPortfolioEditorView;
import eportfolio.view.ImageComponentView;
import eportfolio.view.SlideEditView;
import eportfolio.view.SlideshowComponentView;
import eportfolio.view.TextComponentView;
import eportfolio.view.VideoComponentView;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTML;

/**
 *
 * @author Linh
 */
public class ComponentController {
    
    
    /**
     * This constructor keeps the UI for later.
     */
    public ComponentController() {
    }
    
    public void processSelectImage(ImageModel imageToEdit, ImageComponentView view) {
	FileChooser imageFileChooser = new FileChooser();
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    imageToEdit.setImage(path, fileName);
	    view.updateImage();
	}
    }
    
    public void processSelectVideo(VideoModel videoToEdit, VideoComponentView view) {
	FileChooser videoFileChooser = new FileChooser();
	
	// LET'S ONLY SEE VIDEO FILES
	FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
	FileChooser.ExtensionFilter flvFilter = new FileChooser.ExtensionFilter("FLV files (*.flv)", "*.FLV");
	FileChooser.ExtensionFilter wmvFilter = new FileChooser.ExtensionFilter("WMV files (*.wmv)", "*.WMV");
        FileChooser.ExtensionFilter aviFilter = new FileChooser.ExtensionFilter("AVI files (*.avi)", "*.AVI");
        FileChooser.ExtensionFilter mpgFilter = new FileChooser.ExtensionFilter("MPG files (*.mpg)", "*.MPG");
        
	videoFileChooser.getExtensionFilters().addAll(mp4Filter, flvFilter, wmvFilter,aviFilter,mpgFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = videoFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    videoToEdit.setVideo(path, fileName);
	    view.updateVideo();
	}
    }
    
    
     public void processSelectSlide(ImageModel imageToEdit, SlideEditView view) {
	FileChooser imageFileChooser = new FileChooser();
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = imageFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    imageToEdit.setImage(path, fileName);
	    view.updateSlideImage();
	}
    }
     /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest(SlideshowComponentView slideshowView) {
	SlideshowModel slideShow = slideshowView.getSlideshow();
	slideShow.addSlide();	
	slideshowView.reloadSlideShowPane();
    }

     /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemoveSlideRequest(SlideshowComponentView slideshowView) {
	SlideshowModel slideShow = slideshowView.getSlideshow();
	slideShow.removeSelectedSlide();
	slideshowView.reloadSlideShowPane();
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to move it up in the slide show.
     */
    public void processMoveSlideUpRequest(SlideshowComponentView slideshowView) {
	SlideshowModel slideShow = slideshowView.getSlideshow();
	slideShow.moveSelectedSlideUp();
        slideshowView.reloadSlideShowPane();
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wises to move it down in the slide show.
     */
    public void processMoveSlideDownRequest(SlideshowComponentView slideshowView) {
	SlideshowModel slideShow = slideshowView.getSlideshow();
	slideShow.moveSelectedSlideDown();
        slideshowView.reloadSlideShowPane();
    }
    
     /**
     * Provides a response for when the user wishes to add a new
     * list point to the list.
     */
    public void processAddListPointRequest(TextComponentView textView) {
	TextModel list = textView.getText();
        ListPointModel listPoint = new ListPointModel("");
	list.addListPoint(listPoint);	
	textView.reloadListPane();
    }

     /**
     * Provides a response for when the user has selected a list point
     * and wishes to remove it from the list.
     */
    public void processRemoveListPointRequest(TextComponentView textView) {
	TextModel list = textView.getText();
	list.removeSelectedListPoint();
	textView.reloadListPane();
    }
    



}
