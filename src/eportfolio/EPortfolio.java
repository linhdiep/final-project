
package eportfolio;

import static eportfolio.StartupConstants.ICON_WINDOW_LOGO;
import static eportfolio.StartupConstants.PATH_IMAGES;
import eportfolio.file.FileManager;
import eportfolio.file.SiteExporter;
import eportfolio.view.EPortfolioEditorView;
import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Linh Diep 
 * final
 */
public class EPortfolio extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    FileManager fileManager = new FileManager();
    SiteExporter siteExporter = new SiteExporter() ;
    
    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    EPortfolioEditorView ui = new EPortfolioEditorView(fileManager,siteExporter);
    @Override
    public void start(Stage primaryStage) throws Exception {
	
	// SET THE WINDOW ICON
	String imagePath = PATH_IMAGES + ICON_WINDOW_LOGO;
	File file = new File(imagePath);
	
	// GET AND SET THE IMAGE
	URL fileURL = file.toURI().toURL();
	Image windowIcon = new Image(fileURL.toExternalForm());
	primaryStage.getIcons().add(windowIcon);
	
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        
        String appTitle = "ePortfolio";

	// NOW START THE UI IN EVENT HANDLING MODE
	 ui.startUI(primaryStage, appTitle);
	
    }
    

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}