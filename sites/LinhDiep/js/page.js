 
//Author: Linh Diep
//// CONSTANTS

var IMG_PATH = "./img/";
var ICON_PATH = "./icons/";
var VIDEO_PATH = "./video/";

// KEEP TRACK OF NUMBER OF COMPONENTS
var img_num = 0;
var header_num = 0;
var list_num = 0;
var para_num = 0;
var slideshow_num = 0;
var video_num = 0;

// DATA FOR SITE INITIALIZATION
var title;
var student_name;
var template_layout;
var template_color;
var banner_file_name;
var banner_path;
var font_family;

// DATA FOR SLIDESHOW
var slide_img_id;
var slide_caption_id;
var j = 0; 
var index = 1;
var timeoutId;
var image = new Array(); 
var caption = new Array();
var img_width;
var scaled_img_height;


//
//
// FUNCTIONS FOR LOADING FILE AND INITIALIZING PAGE
//
//


function loadData(currentPage) {
    var jsonFile = "./data/json_file.json";
    $.getJSON(jsonFile, function(json) {
    student_name = json.student_name;
    var currentJson =json.pages[currentPage-1];
    loadPage(currentJson);
    initPage(currentPage,json.pages.length);
    });
}



function loadPage(data) {
	title = data.title;
	template_layout = data.template_layout ;
	template_color = data.template_color;
	banner_file_name = data.banner_file_name ;
	banner_path = data.banner_path;
	footer = data.footer;
	font_family = data.fontFamily;
	
    for (var i = 0; i < data.components.length; i++) {
	var component = data.components[i].component;
	switch (component.toLowerCase()) { 
	case 'image': 
		imgComponent(data.components[i]);
	
		break;
	case 'text': 
		switch (data.components[i].type.toLowerCase()) { 
		case 'header':
			headerComponent(data.components[i]);
			break;
		case 'list':
			listComponent(data.components[i]);
			break;
		case 'paragraph':
			paragraphComponent(data.components[i]);
			break;
		}
		break;
	case 'slideshow': 
		slideshowComponent(data.components[i]);
		break;		
	case 'video': 
		videoComponent(data.components[i]);
		break;
	}
    }  
 }


function initPage(pageNum, totalPage) {
	//STUDENT NAME
    $("#student-name").html(student_name);
	
	//TITLE
	$("#title").html(title);
	
	//NAVIGATION BAR
	pageNodes = [];
	for (var i = 0; i < totalPage; i++) {
		if (i===0){
			pageNodes[i] = '<li class="active"><a href="'+(i+1)+'.html">Page '+(i+1)+'</a></li>';
		}else{
			pageNodes[i] = '<li><a href="'+(i+1)+'.html">Page '+(i+1)+'</a></li>';
		}
		
	}
	var nav = pageNodes.join( " " );
	$("#nav").append(nav) ;
	
	//FONT FAMILY
	loadFont(font_family);
	$('body').css("font-family",font_family);
	
	//LAYOUT TEMPLATE
	layout = '<link rel="stylesheet" type="text/css" href="./css/'+template_layout+'.css">';
	$("head").append(layout);
	
	//COLOR SCHEME
	switch (template_color.toLowerCase()) { 
	case 'white': 
		switchWhite();
		break;
	case 'black': 
		switchBlack();
		break;
	case 'red': 
		switchRed();
		break;		
	case 'green': 
		switchGreen();
		break;
	case 'blue': 
		switchBlue();
		break;
	}
	
	//BANNER
	$("#banner-img").attr("src", IMG_PATH + banner_file_name);
    
	// FOOTER
	$("#footer").html(footer);
}

function loadFont(font_family){
	var font;
	switch (font_family) { 
	case "'Slabo 27px', serif": 
		font = "<link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>";
		break;
	case "'Open Sans', sans-serif": 
		font = "<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>";
		break;
	case "'Titillium Web', sans-serif": 
		font = "<link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>";
		break;		
	case "'Lobster', cursive": 
		font = "<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>";
		break;
	case "'Pacifico', cursive": 
		font = "<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>";
		break;
	}
	
	$("head").append(font);
}
//
//
// FUNCTIONS FOR SWITCHING COLOR SCHEMES
//
//

function switchWhite() {
	 $('a').css({"color":'#878787'});
	 $('body').css({ "background-color": '#CCCACA', "color":'#838383'});
	  $('#nav-bar').css({ "background-color": '#CCCACA', "color":'#838383'});
	  $('#banner').css({ "background-color": '#EFEFEF', "color":'#838383'});
	  $('#content').css({ "background-color": '#FCFCFC', "color":'#838383'});
	  $('#footer').css({ "background-color": '#B4B3B3', "color": '#6B6B6B'});
}
function switchBlack() {
	$('a').css({"color":'#C9C9C9'});
	$('body').css({ "background-color": '#555353', "color":'#EFECEC'});
	$('#nav-bar').css({ "background-color": '#555353', "color":'#EFECEC'});
	$('#banner').css({ "background-color": '#807A7A', "color":'#EFECEC'});
	$('#content').css({ "background-color": '#6B6B6B', "color":'#EFECEC'});
	$('#footer').css({ "background-color": '#E7E4E4', "color": '#E7ACAC'});
}
function switchBlue() {
    $('a').css({"color":'#5C85D6'});
    $('body').css({ "background-color": '#C5D3E6', "color":'#7A9AC5'});
    $('#nav-bar').css({ "background-color": '#C5D3E6', "color":'#7A9AC5'});
    $('#banner').css({ "background-color": '#F4F7FA', "color":'#7A9AC5'});
    $('#content').css({ "background-color": '#FCFDFE', "color":'#7A9AC5'});
    $('#footer').css({ "background-color": '#7A9AC5', "color": '#E4EBF4'});
}

function switchGreen() { 
  $('a').css({"color":'#009933'});
  $('body').css({ "background-color": '#C1EDA1', "color":'#001A00'});
  $('#nav-bar').css({ "background-color": '#C1EDA1', "color":'#001A00'});
  $('#banner').css({ "background-color": '#DFF6CF', "color":'#001A00'});
  $('#content').css({ "background-color": '#F8FDF5', "color":'#001A00'});
  $('#footer').css({ "background-color": '#406E4E', "color": '#DCF5CA'});
}

function switchRed() {
  $('a').css({"color":'#A80000'});
  $('body').css({ "background-color": '#ED7D7D', "color":'#601919'});
  $('#nav-bar').css({ "background-color": '#ED7D7D', "color":'#601919'});
  $('#banner').css({ "background-color": '#D46A6A', "color":'#601919'});
  $('#content').css({ "background-color": '#FFAAAA', "color":'#601919'});
  $('#footer').css({ "background-color": '#A23C3C', "color": '#E7ACAC'});
}

//
//
// FUNCTIONS FOR COMPONENTS
//
//

function imgComponent(data){
	img_num++;
	var img_id = "img_"+img_num;
	var img_cap_id = "img_cap"+img_num;
	$('<img>', {id:img_id})
		  .attr("src", IMG_PATH + data.image_file_name )
		  .css({"width": data.width,"height": data.height, "float": data.position})
		  .appendTo("#content");
	$('<h3>',{id:img_cap_id})
		.append(data.caption)
		.appendTo("#content");
}

function headerComponent(data){
	header_num++;
	var tag = data.fontSize;
	var header_id = "header_"+ header_num;
	loadFont(data.fontFamily);
	$('<'+tag+'>',{id:header_id})
		.append(data.content)
		.css({"font-family": data.fontFamily, "font-style": data.fontStyle})
		.appendTo("#content");
}

function listComponent(data){
	list_num++;
	var list_id = "list_"+ list_num;
	listElem = [];
	for (var i = 0; i < data.content.length; i++) {
		listElem[i] = '<li>'+data.content[i].elem+'</li>';
	}
	var listElem_html = listElem.join( "</br>" );
	$('<ul>',{id:list_id})
		.append(listElem_html)
		.css({"font-family": data.fontFamily, "font-size" : data.fontSize, "font-style": data.fontStyle})
		.appendTo("#content");
}

function paragraphComponent(data){
	para_num++;
	var para_id = "para_"+ para_num;
	loadFont(data.fontFamily);
	$('<p>',{id:para_id})
		.append(data.content)
		.css({"font-family": data.fontFamily, "font-size" : data.fontSize, "font-style": data.fontStyle})
		.appendTo("#content");
	
}

function videoComponent(data){
	video_num++;
	var video_filename = data.video_file_name;
	var video_type = video_filename.substring(video_filename.lastIndexOf('.')+1);
	var video_id = "video_"+ video_num;
	var vid_cap_id = "vid_cap"+ video_num;
	var video_src = '<source src="'+ VIDEO_PATH + video_filename + '" type="video/'+video_type+'">' 
					+ 'Your browser does not support HTML5 videos.';
	$('<video>',{id:video_id, controls: true})
		.append(video_src)
		.css({"width": data.width,"height": data.height})
		.appendTo("#content");
	$('<h3>',{id:vid_cap_id})
		.append(data.caption)
		.appendTo("#content");
	
}


function slideshowComponent(data){
        img_width = data.width;
        scaled_img_height = data.height;
	slideshow_num++;
	var slideshow_title_id = "slideshow_title"+ slideshow_num;
	var slide_img_id = "slide_img_"+ slideshow_num;
	var slide_caption_id = "slide_caption_"+ slideshow_num;
        controls_id = "slideshow_controls_"+ slideshow_num;
        prev_id = "prev_"+ slideshow_num;
        next_id = "next_"+ slideshow_num;
        play_id = "play_"+ slideshow_num;
        pause_id = "pause_"+ slideshow_num;
    
	$('<h2>',{id:slideshow_title_id})
		.append(data.title)
		.appendTo("#content");
		
	$('<img>', {id:slide_img_id})
			.attr("src", IMG_PATH + data.slides[0].image_file_name)
                        .css({"width": img_width,"height": scaled_img_height})
                        .one("load", function() {
                        autoScaleImage();
                        })
			.appendTo("#content");
		  
	
	$('<div>', {id:slide_caption_id})
			.append( data.slides[0].caption)
			.appendTo("#content");
			
	$('<div>',{id:'slideshow_controls'})
			.append('<input id="'+prev_id+'" type="image" src="./icons/Previous.png" onclick="processPreviousRequest()">'
                                +'<input id="'+play_id+'" type="image" src="./icons/Play.png" onclick="processPlayRequest()">'
                                +'<input id="'+pause_id+'" type="image" style="display:none" src="./icons/Pause.png" onclick="processPauseRequest()">'                    
                                +'<input id="'+next_id+'" type="image" src="./icons/Next.png" onclick="processNextRequest()">')                       
			.appendTo("#content");
	
	for(var i = 0; i < data.slides.length; i++){
		image[j] = IMG_PATH + data.slides[i].image_file_name;
        caption[j] = data.slides[i].caption;
		j++;
	}
}

	function processPreviousRequest(){
                clearTimeout ( timeoutId );   
		$("#"+slide_img_id).attr("src", image[index]);
		$("#"+slide_caption_id).text(caption[index]);
		$("#"+slide_img_id).one("load", function() {
                    autoScaleImage();
                });
		if(index > 0 ) { index--;}  
		else  { index = j-1; } 
    }

    function processNextRequest(){
        clearTimeout ( timeoutId );   
        $("#"+slide_img_id).attr("src", image[index]);
	$("#"+slide_caption_id).text(caption[index]);
        $("#"+slide_img_id).one("load", function() {
	    autoScaleImage();
	});
	if(index < j-1 ) { index++;}  
	else  { index = 0; }   
    }
    
    function processPlayRequest(){
        $('#'+ play_id).hide();
        $('#'+ pause_id).show();  
        play();
    }
    
    function play(){
	$("#"+slide_img_id).attr("src", image[index]);
	$("#"+slide_caption_id).text(caption[index]);
        $("#"+slide_img_id).one("load", function() {
	    autoScaleImage();
	});
	if(index < j-1 ) { index++;}  
	else  { index = 0; }  
	timeoutId = setTimeout(play,3000);
    }
    
    function processPauseRequest(){
        clearTimeout (timeoutId);   
        $('#'+ pause_id).hide();
        $('#'+ play_id).show();
    }
function autoScaleImage() {
	var origHeight = $("#"+slide_img_id).height();
	var scaleFactor = scaled_img_height/origHeight;
	var origWidth = $("#"+slide_img_id).width();
	var scaledWidth = origWidth * scaleFactor;
	$("#"+slide_img_id).height(scaled_img_height);
	$("#"+slide_img_id).width(scaledWidth);
	var left = (img_width-scaledWidth)/2;
	$("#"+slide_img_id).css("left", left);
}